import datetime
from ventas.models import Venta, Envio, Factura
from main.models import Usuario
from afiliados.models import Automovil
from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse
from reportlab.pdfgen import canvas
from reportlab.lib.units import mm
from reportlab.lib.pagesizes import letter, landscape
from reportlab.platypus import Table, Paragraph
from reportlab.lib import colors
from reportlab.lib.styles import getSampleStyleSheet


def persona(request):
    return render(request, 'persona.html')


def persona_get(request):
    opciones = request.GET.getlist('options')
    num_cols = len(opciones)
    if num_cols <= 0:
        return print_default()
    response = HttpResponse(content_type='application/pdf')
    response['Content-Disposition'] = 'filename = "reporte.pdf"'
    # Create the PDF object, using the response object as its "file."
    p = canvas.Canvas(response, pagesize=landscape(letter))
    width, height = landscape(letter)
    p.setTitle("Personas")
    p.setFont('Helvetica-Bold', 16)
    p.drawCentredString(139.7*mm, 205*mm, "Reporte de personas")
    cabeceras = []
    for opcion in opciones:
        cabeceras.append({
            'ci': "C.I.",
            'nombres': "Nombres",
            'apellidos': "Apellidos",
            'direccion': "Direccion",
            'telefono': "Telefono",
            'tipo': "Tipo",
            'estado': 'Estado',
            'automovil': "Automovil"
        }[opcion])
    styles = getSampleStyleSheet()
    data = [cabeceras]
    for user in Usuario.objects.all():
        nueva_fila = []
        for opcion in opciones:
            nueva_fila.append({
                'ci': Paragraph(user.persona.ci, styles['Normal']),
                'nombres': Paragraph(user.persona.nombres, styles['Normal']),
                'apellidos': Paragraph(user.persona.apellidos, styles['Normal']),
                'direccion': Paragraph(user.persona.direccion, styles['Normal']),
                'telefono': user.persona.telefono,
                'tipo': Paragraph(user.tipo, styles['Normal']),
                'estado': Paragraph(user.afiliado.estado if hasattr(user, 'afiliado') else 'No definido',
                                    styles['Normal']),
                'automovil': Paragraph(user.afiliado.automovil.placa
                                       if hasattr(user, 'afiliado') and hasattr(user.afiliado, 'automovil')
                                       else 'No definido', styles['Normal'])
            }[opcion])
        data.append(nueva_fila)
    colWidths = [(260 / num_cols) * mm for i in range(num_cols)]
    table = Table(data, colWidths=colWidths)
    table.setStyle([
        ('BACKGROUND', (0, 0), (-1, 0), colors.lightgrey),
        ("VALIGN", (0, 0), (-1, -1), "MIDDLE"),
        ("ALIGN", (0, 0), (-1, 0), "CENTER"),
        ("FONT", (0, 0), (-1, 0), "Helvetica-Bold"),
        ('ALIGN', (0, 1), (-1, -1), 'LEFT'),
        ('INNERGRID', (0, 0), (-1, -1), 0.25, colors.black),
        ('BOX', (0, 0), (-1, -1), 0.25, colors.black)],
    )

    wt, ht = table.wrapOn(p, width, height)
    table.drawOn(p, 10 * mm, (200 * mm)-ht)

    p.showPage()
    p.save()
    return response


def automovil(request):
    return render(request, 'automovil.html')


def automovil_get(request):
    opciones = request.GET.getlist('options')
    num_cols = len(opciones)
    if num_cols <= 0:
        return print_default()
    response = HttpResponse(content_type='application/pdf')
    response['Content-Disposition'] = 'filename = "reporte.pdf"'
    # Create the PDF object, using the response object as its "file."
    p = canvas.Canvas(response, pagesize=landscape(letter))
    width, height = landscape(letter)
    p.setTitle("Personas")
    p.setFont('Helvetica-Bold', 16)
    p.drawCentredString(139.7*mm, 205*mm, "Reporte de automoviles")
    cabeceras = []
    for opcion in opciones:
        cabeceras.append({
            'propietario': "PROPIETARIO",
            'marca': "MARCA",
            'tipo': "TIPO",
            'placa': "PLACA",
            'clase': "CLASE",
            'asientos': "ASIENTOS",
            'color': 'COLOR'
        }[opcion])
    data = [cabeceras]
    style = getSampleStyleSheet()
    for auto in Automovil.objects.all():
        nueva_fila = []
        for opcion in opciones:
            nueva_fila.append({
                'propietario': Paragraph(auto.conductor.usuario.persona.nombres
                                         if hasattr(auto, 'conductor') and auto.conductor is not None
                                         else 'No definido', style['Normal']),
                'marca': Paragraph(auto.marca, style['Normal']),
                'tipo': Paragraph(auto.tipo, style['Normal']),
                'placa': Paragraph(auto.placa, style['Normal']),
                'clase': Paragraph(auto.clase, style['Normal']),
                'asientos': auto.plazas,
                'color': Paragraph(auto.color, style['Normal'])
            }[opcion])
        data.append(nueva_fila)
    colWidths = [(260 / num_cols) * mm for i in range(num_cols)]
    table = Table(data, colWidths=colWidths)
    table.setStyle([
        ('BACKGROUND', (0, 0), (-1, 0), colors.lightgrey),
        ("VALIGN",  (0, 0), (-1, -1), "MIDDLE"),
        ("ALIGN", (0, 0), (-1, 0), "CENTER"),
        ("FONT", (0, 0), (-1, 0), "Helvetica-Bold"),
        ('ALIGN', (0, 1), (-1, -1), 'LEFT'),
        ('INNERGRID', (0, 0), (-1, -1), 0.25, colors.black),
        ('BOX', (0, 0), (-1, -1), 0.25, colors.black)],
    )

    wt, ht = table.wrapOn(p, width, height)
    table.drawOn(p, 10 * mm, (200 * mm)-ht)

    p.showPage()
    p.save()
    return response


def envios(request):
    return render(request, 'envios.html')


def envios_get(request):
    opciones = request.GET.getlist('options')
    num_cols = len(opciones)
    if num_cols <= 0:
        return print_default()
    str_start_date = request.GET.get('start_date', datetime.datetime.now().strftime("%Y-%m-%d"))
    str_end_date = request.GET.get('end_date', datetime.datetime.now().strftime("%Y-%m-%d"))
    response = HttpResponse(content_type='application/pdf')
    response['Content-Disposition'] = 'filename = "reporte.pdf"'
    # Create the PDF object, using the response object as its "file."
    p = canvas.Canvas(response, pagesize=landscape(letter))
    width, height = landscape(letter)
    p.setTitle("Envios")
    p.setFont('Helvetica-Bold', 16)
    p.drawCentredString(139.7*mm, 205*mm, "Reporte de envios")
    p.setFont('Helvetica-Bold', 10)
    p.drawCentredString(139.7 * mm, 200 * mm, str_start_date + " - " + str_end_date)
    cabeceras = []
    style = getSampleStyleSheet()
    hstyle = style['Normal']
    hstyle.fontName = 'Helvetica-Bold'
    for opcion in opciones:
        cabeceras.append({
            'tipo': Paragraph("TIPO", style['Normal']),
            'guia': Paragraph("GUIA", style['Normal']),
            'fecha': Paragraph("FECHA", style['Normal']),
            'remitente': Paragraph("REMITENTE", style['Normal']),
            'consignatario': Paragraph("CONSIGNATARIO", style['Normal']),
            'costo': Paragraph("COSTO", style['Normal']),
            'conductor': Paragraph("CONDUCTOR", style['Normal']),
            'automovil': Paragraph("AUTOMOVIL", style['Normal']),
            'factura': Paragraph("FACTURA", style['Normal']),
            'contenido': Paragraph("CONTENIDO", style['Normal']),
            'origen': Paragraph("ORIGEN", style['Normal']),
            'destino': Paragraph("DESTINO", style['Normal']),
            'responsable': Paragraph("RESPONSABLE", style['Normal']),
        }[opcion])
    data = [cabeceras]
    hstyle.fontName = 'Helvetica'
    for envio in Envio.objects.filter(venta__fecha_hora__date__range=[str_start_date, str_end_date]):
        nueva_fila = []
        for opcion in opciones:
            nueva_fila.append({
                'tipo': Paragraph(envio.tipo, style['Normal']),
                'guia': Paragraph(envio.nro_guia, style['Normal']),
                'fecha': Paragraph(envio.venta.fecha_hora.strftime("%d/%m/%Y %H:%M"), style['Normal']),
                'remitente': Paragraph(envio.venta.cliente.nombre, style['Normal']),
                'consignatario': Paragraph(envio.nombre_consignatario, style['Normal']),
                'conductor': Paragraph(envio.venta.lista.afiliado.usuario.persona.nombres + ' '
                                         + envio.venta.lista.afiliado.usuario.persona.apellidos,
                                         style['Normal']),
                'automovil': Paragraph(envio.venta.lista.automovil.placa, style['Normal']),
                'factura': envio.venta.factura.numero if hasattr(envio.venta, 'factura') else 'SF',
                'costo': envio.venta.total,
                'contenido': Paragraph(envio.detalle, style['Normal']),
                'origen': Paragraph(envio.venta.origen, style['Normal']),
                'destino': Paragraph(envio.venta.destino, style['Normal']),
                'responsable': Paragraph(envio.venta.responsable.usuario.persona.nombres + ' '
                                         + envio.venta.responsable.usuario.persona.apellidos, style['Normal']),
            }[opcion])
        data.append(nueva_fila)
    colWidths = [(260 / num_cols) * mm for i in range(num_cols)]
    table = Table(data, colWidths=colWidths)
    table.setStyle([
        ('BACKGROUND', (0, 0), (-1, 0), colors.lightgrey),
        ("VALIGN", (0, 0), (-1, -1), "MIDDLE"),
        ("ALIGN", (0, 0), (-1, 0), "CENTER"),
        ("FONT", (0, 0), (-1, 0), "Helvetica-Bold"),
        ('ALIGN', (0, 1), (-1, -1), 'LEFT'),
        ('INNERGRID', (0, 0), (-1, -1), 0.25, colors.black),
        ('BOX', (0, 0), (-1, -1), 0.25, colors.black)],
    )

    wt, ht = table.wrapOn(p, width, height)
    table.drawOn(p, 10 * mm, (195 * mm)-ht)

    p.showPage()
    p.save()
    return response


def facturacion(request):
    return render(request, 'facturacion.html')


def facturacion_get(request):
    opciones = request.GET.getlist('options')
    num_cols = len(opciones)
    if num_cols <= 0:
        return print_default()
    str_start_date = request.GET.get('start_date', datetime.datetime.now().strftime("%Y-%m-%d"))
    str_end_date = request.GET.get('end_date', datetime.datetime.now().strftime("%Y-%m-%d"))
    response = HttpResponse(content_type='application/pdf')
    response['Content-Disposition'] = 'filename = "reporte.pdf"'
    # Create the PDF object, using the response object as its "file."
    p = canvas.Canvas(response, pagesize=landscape(letter))
    width, height = landscape(letter)
    p.setTitle("Ventas")
    p.setFont('Helvetica-Bold', 16)
    p.drawCentredString(139.7*mm, 205*mm, "Reporte de facturas")
    p.setFont('Helvetica-Bold', 10)
    p.drawCentredString(139.7 * mm, 200*mm, str_start_date + " - " + str_end_date)
    cabeceras = []
    for opcion in opciones:
        cabeceras.append({
            'fecha': "FECHA",
            'numero': "FACTURA",
            'lugar': "LUGAR",
            'nombre': "NOMBRE",
            'nit': "NIT",
            'telefono': "TELEFONO",
            'detalle': "DETALLE",
            'total': "TOTAL"
        }[opcion])
    style = getSampleStyleSheet()
    data = [cabeceras]
    for factura in Factura.objects.filter(fecha_hora__date__range=[str_start_date, str_end_date]):
        nueva_fila = []
        for opcion in opciones:
            nueva_fila.append({
                'fecha': Paragraph(factura.fecha_hora.strftime("%d/%m/%Y %H:%M"), style['Normal']),
                'numero': factura.numero,
                'lugar': Paragraph(factura.dosificacion.lugar.nombre, style['Normal']),
                'nombre': Paragraph(factura.nombre, style['Normal']),
                'nit': Paragraph(factura.ci_nit, style['Normal']),
                'telefono': factura.venta.cliente.cel,
                'detalle': factura.venta.tipo,
                'total': factura.importe,
            }[opcion])
        data.append(nueva_fila)
    colWidths = [(260 / num_cols) * mm for i in range(num_cols)]
    table = Table(data, colWidths=colWidths)
    table.setStyle([
        ('BACKGROUND', (0, 0), (-1, 0), colors.lightgrey),
        ("VALIGN", (0, 0), (-1, -1), "MIDDLE"),
        ("ALIGN", (0, 0), (-1, 0), "CENTER"),
        ("FONT", (0, 0), (-1, 0), "Helvetica-Bold"),
        ('ALIGN', (0, 1), (-1, -1), 'LEFT'),
        ('INNERGRID', (0, 0), (-1, -1), 0.25, colors.black),
        ('BOX', (0, 0), (-1, -1), 0.25, colors.black)],
    )

    wt, ht = table.wrapOn(p, width, height)
    table.drawOn(p, 10 * mm, (195 * mm)-ht)

    p.showPage()
    p.save()
    return response


def faltas_atrasos(request):
    return render(request, 'faltas_atrasos.html')


def faltas_atrasos_get(request):
    opciones = request.GET.getlist('options')
    ci = request.GET.get('ci')
    num_cols = len(opciones)
    try:
        user = Usuario.objects.get(persona__ci=ci)
    except (Usuario.DoesNotExist, Usuario.MultipleObjectsReturned) as e:
        user = None
    if num_cols <= 0 or user is None:
        return print_default()
    str_start_date = request.GET.get('start_date', datetime.datetime.now().strftime("%Y-%m-%d"))
    str_end_date = request.GET.get('end_date', datetime.datetime.now().strftime("%Y-%m-%d"))
    response = HttpResponse(content_type='application/pdf')
    response['Content-Disposition'] = 'filename = "reporte.pdf"'
    # Create the PDF object, using the response object as its "file."
    p = canvas.Canvas(response, pagesize=landscape(letter))
    width, height = landscape(letter)
    p.setTitle("Ventas")
    p.setFont('Helvetica-Bold', 16)
    p.drawCentredString(139.7 * mm, 205 * mm, "Reporte de faltas y atrasos")
    p.setFont('Helvetica-Bold', 10)
    p.drawCentredString(139.7 * mm, 200 * mm, str_start_date + " - " + str_end_date)
    p.drawString(28, 195 * mm, "De: ")
    p.setFont('Helvetica', 10)
    p.drawString(48, 195*mm, "%s %s - %s" % (user.persona.nombres, user.persona.apellidos, user.persona.ci))
    if hasattr(user, 'secretaria'):
        cabeceras = []
        for opcion in opciones:
            cabeceras.append({
                                 'fecha': "FECHA",
                                 'hora_ingreso': "HRA. INGRESO",
                                 'hora_salida': "HRA. SALIDA",
                                 'tiempo': "TIEMPO NO TR.",
                                 'dias': "DIA NO TR.",
                                 'suma_tiempo': "SUM. TIEMPO",
                                 'suma_dias': "SUM. DIAS",
                             }[opcion])
        style = getSampleStyleSheet()
        data = [cabeceras]
        suma_tiempo = 0
        suma_dias = 0
        for item in user.secretaria.asignaciones.filter(fecha__range=[str_start_date, str_end_date]).order_by('fecha', 'hora_ingreso'):
            nueva_fila = []
            hora_ingreso = item.hora_ingreso
            hora_ingreso_reg = item.hora_ingreso_reg
            tiempo = 0
            dia = 0
            if hora_ingreso_reg is not None:
                t1 = datetime.timedelta(hours=hora_ingreso.hour, minutes=hora_ingreso.minute)
                t2 = datetime.timedelta(hours=hora_ingreso_reg.hour, minutes=hora_ingreso_reg.minute)
                tiempo = (t2 - t1).total_seconds()
                suma_tiempo += tiempo

            else:
                dia = 1
                suma_dias += 1
            for opcion in opciones:
                nueva_fila.append({
                    'fecha': Paragraph(item.fecha.strftime("%d/%m/%Y"), style['Normal']),
                    'hora_ingreso': Paragraph("%s (%s)" % (item.hora_ingreso.strftime("%H:%M"),
                                                           item.hora_ingreso_reg.strftime("%H:%M")
                                                           if item.hora_ingreso_reg is not None
                                                           else 'Falta'), style['Normal']),
                    'hora_salida': Paragraph("%s (%s)" % (item.hora_salida.strftime("%H:%M"),
                                                          item.hora_salida_reg.strftime("%H:%M")
                                                          if item.hora_salida_reg is not None
                                                          else 'Falta'), style['Normal']),
                    'tiempo': Paragraph("%d min." % (tiempo // 60), style['Normal']),
                    'dias': dia,
                    'suma_tiempo': Paragraph("%d min." % (suma_tiempo // 60), style['Normal']),
                    'suma_dias': suma_dias}[opcion])
            data.append(nueva_fila)

        colWidths = [(260 / num_cols) * mm for i in range(num_cols)]
        table = Table(data, colWidths=colWidths)
        table.setStyle([
            ('BACKGROUND', (0, 0), (-1, 0), colors.lightgrey),
            ("VALIGN", (0, 0), (-1, -1), "MIDDLE"),
            ("ALIGN", (0, 0), (-1, 0), "CENTER"),
            ("FONT", (0, 0), (-1, 0), "Helvetica-Bold"),
            ('ALIGN', (0, 1), (-1, -1), 'LEFT'),
            ('INNERGRID', (0, 0), (-1, -1), 0.25, colors.black),
            ('BOX', (0, 0), (-1, -1), 0.25, colors.black)],
        )

        wt, ht = table.wrapOn(p, width, height)
        table.drawOn(p, 10 * mm, (192 * mm) - ht)
    p.showPage()
    p.save()
    return response


def ventas(request):
    return render(request, 'ventas.html')


def ventas_get(request):
    opciones = request.GET.getlist('options')
    num_cols = len(opciones)
    if num_cols <= 0:
        return print_default()
    str_start_date = request.GET.get('start_date', datetime.datetime.now().strftime("%Y-%m-%d"))
    str_end_date = request.GET.get('end_date', datetime.datetime.now().strftime("%Y-%m-%d"))
    response = HttpResponse(content_type='application/pdf')
    response['Content-Disposition'] = 'filename = "reporte.pdf"'
    # Create the PDF object, using the response object as its "file."
    p = canvas.Canvas(response, pagesize=landscape(letter))
    width, height = landscape(letter)
    p.setTitle("Ventas")
    p.setFont('Helvetica-Bold', 16)
    p.drawCentredString(139.7*mm, 205*mm, "Reporte de ventas")
    p.setFont('Helvetica-Bold', 10)
    p.drawCentredString(139.7 * mm, 200*mm, str_start_date + " - " + str_end_date)
    cabeceras = []
    for opcion in opciones:
        cabeceras.append({
            'fecha': "FECHA",
            'tipo': "TIPO",
            'costo': "COSTO",
            'conductor': "CONDUCTOR",
            'automovil': "AUTOMOVIL",
            'factura': "FACTURA",
            'origen': "ORIGEN",
            'destino': "DESTINO",
            'responsable': "RESPONSABLE",
        }[opcion])
    style = getSampleStyleSheet()
    data = [cabeceras]
    for venta in Venta.objects.filter(fecha_hora__date__range=[str_start_date, str_end_date]):
        nueva_fila = []
        for opcion in opciones:
            nueva_fila.append({
                'fecha': Paragraph(venta.fecha_hora.strftime("%d/%m/%Y %H:%M"), style['Normal']),
                'tipo': Paragraph(venta.tipo, style['Normal']),
                'costo': venta.total,
                'conductor': Paragraph(venta.lista.afiliado.usuario.persona.nombres + ' '
                                       + venta.lista.afiliado.usuario.persona.apellidos, style['Normal']),
                'automovil': Paragraph(venta.lista.automovil.placa, style['Normal']),
                'factura': venta.factura.numero if hasattr(venta, 'factura') else 'SF',
                'origen': Paragraph(venta.origen, style['Normal']),
                'destino': Paragraph(venta.destino, style['Normal']),
                'responsable': Paragraph(venta.responsable.usuario.persona.nombres + ' ' +
                                         venta.responsable.usuario.persona.apellidos, style['Normal'])
            }[opcion])
        data.append(nueva_fila)
    colWidths = [(260 / num_cols) * mm for i in range(num_cols)]
    table = Table(data, colWidths=colWidths)
    table.setStyle([
        ('BACKGROUND', (0, 0), (-1, 0), colors.lightgrey),
        ("VALIGN", (0, 0), (-1, -1), "MIDDLE"),
        ("ALIGN", (0, 0), (-1, 0), "CENTER"),
        ("FONT", (0, 0), (-1, 0), "Helvetica-Bold"),
        ('ALIGN', (0, 1), (-1, -1), 'LEFT'),
        ('INNERGRID', (0, 0), (-1, -1), 0.25, colors.black),
        ('BOX', (0, 0), (-1, -1), 0.25, colors.black)],
    )

    wt, ht = table.wrapOn(p, width, height)
    table.drawOn(p, 10 * mm, (195 * mm)-ht)

    p.showPage()
    p.save()
    return response


def ventas_lista(request):
    lugar_actual = request.user.secretaria.lugar.nombre
    user = request.user
    inicio_def = datetime.datetime.now().strftime("%Y-%m-%d 00:00")
    fin_def = datetime.datetime.now().strftime("%Y-%m-%d 23:59")
    inicio = request.GET.get('fecha_inicio', inicio_def)
    fin = request.GET.get('fecha_fin', fin_def)
    lista = Venta.objects.filter(origen=lugar_actual, responsable=user.secretaria, fecha_hora__range=[inicio, fin]).order_by('-fecha_hora')
    return render(request, 'lista_ventas.html', {'lista': lista, 'inicio': inicio, 'fin': fin})


def ventas_lista_get(request):
    lugar_actual = request.user.secretaria.lugar.nombre
    user = request.user
    inicio_def = datetime.datetime.now().strftime("%Y-%m-%d %H:%M")
    fin_def = datetime.datetime.now().strftime("%Y-%m-%d %H:%M")
    inicio = request.GET.get('fecha_inicio', inicio_def)
    fin = request.GET.get('fecha_fin', fin_def)
    lista = Venta.objects.filter(origen=lugar_actual, responsable=user.secretaria,
                                 fecha_hora__range=[inicio, fin]).order_by('-fecha_hora')
    response = HttpResponse(content_type='application/pdf')
    response['Content-Disposition'] = 'filename = "reporte.pdf"'
    # Create the PDF object, using the response object as its "file."
    p = canvas.Canvas(response, pagesize=landscape(letter))
    width, height = landscape(letter)
    p.setTitle("Ventas")
    p.setFont('Helvetica-Bold', 16)
    p.drawCentredString(139.7 * mm, 205 * mm, "Reporte de ventas")
    p.setFont('Helvetica-Bold', 10)
    p.drawCentredString(139.7 * mm, 200 * mm, inicio[:10] + " - " + fin[:10])

    p.drawString(30, 193 * mm, 'Responsable:')
    p.setFont('Helvetica', 10)
    p.drawString(100, 193 * mm, "%s %s (%s)" % (user.persona.nombres, user.persona.apellidos, user.usuario))

    cabeceras = ['FECHA', 'TIPO', 'CI', 'NOMBRE', 'ORIGEN', 'DESTINO', 'COSTO']
    style = getSampleStyleSheet()
    data = [cabeceras]
    for venta in lista:
        nueva_fila = [
            Paragraph(venta.fecha_hora.strftime("%d/%m/%Y %H:%M"), style['Normal']),
            Paragraph(venta.tipo, style['Normal']),
            Paragraph(venta.cliente.ci_nit, style['Normal']),
            Paragraph(venta.cliente.nombre, style['Normal']),
            Paragraph(venta.origen, style['Normal']),
            Paragraph(venta.destino, style['Normal']),
            venta.total
        ]
        data.append(nueva_fila)
    num_cols = 7
    colWidths = [(260 / num_cols) * mm for i in range(num_cols)]
    table = Table(data, colWidths=colWidths)
    table.setStyle([
        ('BACKGROUND', (0, 0), (-1, 0), colors.lightgrey),
        ("VALIGN", (0, 0), (-1, -1), "MIDDLE"),
        ("ALIGN", (0, 0), (-1, 0), "CENTER"),
        ("FONT", (0, 0), (-1, 0), "Helvetica-Bold"),
        ('ALIGN', (0, 1), (-1, -1), 'LEFT'),
        ('INNERGRID', (0, 0), (-1, -1), 0.25, colors.black),
        ('BOX', (0, 0), (-1, -1), 0.25, colors.black)],
    )

    wt, ht = table.wrapOn(p, width, height)
    table.drawOn(p, 10 * mm, (190 * mm) - ht)

    p.showPage()
    p.save()
    return response


def pdf_envio(request, pk):
    venta = get_object_or_404(Venta, pk=pk)

    response = HttpResponse(content_type='application/pdf')
    response['Content-Disposition'] = 'filename = "envio.pdf"'
    # Create the PDF object, using the response object as its "file."
    p = canvas.Canvas(response, pagesize=(80*mm, 140*mm))
    p.setTitle("Envio")
    draw_header(p, 30)

    y = 15

    p.setFont('Helvetica-Bold', 8.5)
    p.drawString(15, y + 220, 'Nro. Guia:')
    p.drawString(15, y + 210, 'Lugar:')
    p.drawString(15, y + 200, 'Fecha:')
    p.drawString(15, y + 190, 'Hora:')

    p.drawString(15, y + 170, 'Conductor:')
    p.drawString(15, y + 160, 'Cel.:')
    p.drawString(15, y + 150, 'Placa:')

    p.drawString(15, y + 130, 'Remitente:')
    p.drawString(15, y + 120, 'Consignatario:')
    p.drawString(15, y + 110, 'CI Consignatario:')
    p.drawString(15, y + 100, 'Telefono:')

    p.drawString(15, y + 80, 'Tipo:')
    p.drawString(15, y + 70, 'Cantidad:')
    p.drawString(15, y + 60, 'Detalle:')
    p.drawString(15, y + 50, 'Costo: (Bs.)')

    p.setFont('Helvetica', 8.5)
    p.drawString(60, y + 220, venta.envio.nro_guia)
    p.drawString(45, y + 210, venta.origen)
    p.drawString(45, y + 200, venta.fecha_hora.strftime("%d/%m/%Y"))
    p.drawString(40, y + 190, venta.fecha_hora.strftime("%H:%M"))

    p.drawString(65, y + 170,
                 venta.lista.afiliado.usuario.persona.nombres + ' ' + venta.lista.afiliado.usuario.persona.apellidos)
    p.drawString(37, y + 160, str(venta.lista.afiliado.usuario.persona.telefono))
    p.drawString(45, y + 150, venta.lista.automovil.placa)

    p.drawString(60, y + 130, venta.cliente.nombre)
    p.drawString(80, y + 120, venta.envio.nombre_consignatario)
    p.drawString(90, y + 110, venta.envio.ci_consignatario)
    p.drawString(60, y + 100, venta.envio.telefono_consignatario)

    p.drawString(40, y + 80, venta.envio.tipo)
    p.drawString(58, y + 70, str(venta.envio.cantidad))
    p.drawString(50, y + 60, venta.envio.detalle)
    p.drawString(70, y + 50, str(venta.total))

    p.drawCentredString(40 * mm, 10, 'Gracias por su preferencia...')

    p.showPage()
    p.save()
    return response


def pdf_boleto(request, pk):
    venta = get_object_or_404(Venta, pk=pk)

    response = HttpResponse(content_type='application/pdf')
    response['Content-Disposition'] = 'filename = "boleto.pdf"'
    # Create the PDF object, using the response object as its "file."
    p = canvas.Canvas(response, pagesize=(80*mm, 130*mm))
    p.setTitle("Boleto")

    draw_header(p, 0)

    y = 0

    p.setFont('Helvetica-Bold', 8.5)
    p.drawString(15, y + 210, 'Lugar:')
    p.drawString(15, y + 200, 'Fecha:')
    p.drawString(15, y + 190, 'Hora:')

    p.drawString(15, y + 170, 'Conductor:')
    p.drawString(15, y + 160, 'Cel.:')
    p.drawString(15, y + 150, 'Placa:')

    p.drawString(15, y + 130, 'Cliente:')
    p.drawString(15, y + 120, 'CI/NIT:')
    p.drawString(15, y + 110, 'Asientos:')
    p.drawString(15, y + 100, 'Destino:')
    p.drawString(15, y + 90, 'Costo: (Bs.)')

    p.setFont('Helvetica', 8.5)
    p.drawString(45, y + 210, venta.origen)
    p.drawString(45, y + 200, venta.fecha_hora.strftime("%d/%m/%Y"))
    p.drawString(40, y + 190, venta.fecha_hora.strftime("%H:%M"))

    p.drawString(65, y + 170, venta.lista.afiliado.usuario.persona.nombres + ' ' + venta.lista.afiliado.usuario.persona.apellidos)
    p.drawString(37, y + 160, str(venta.lista.afiliado.usuario.persona.telefono))
    p.drawString(45, y + 150, venta.lista.automovil.placa)

    p.drawString(55, y + 130, venta.cliente.nombre)
    p.drawString(50, y + 120, venta.cliente.ci_nit)
    p.drawString(60, y + 110, str(venta.boleto.plazas))
    p.drawString(55, y + 100, venta.destino)
    p.drawString(70, y + 90, str(venta.total))

    p.setFont('Helvetica', 8)
    p.drawCentredString(40*mm, 10, 'Gracias por su preferencia...')
    p.showPage()
    p.save()
    return response


def print_default():
    response = HttpResponse(content_type='application/pdf')
    response['Content-Disposition'] = 'filename = "boleto.pdf"'
    # Create the PDF object, using the response object as its "file."
    p = canvas.Canvas(response, pagesize=(80 * mm, 130 * mm))
    p.setTitle("Default")
    p.setFont("Helvetica-Bold", 14)

    p.drawString(40, 340, "Sindicato de transporte")
    p.drawString(52, 325, '"Rapiditos del Sur"')
    p.showPage()
    p.save()
    return response


def draw_header(canv, y):
    canv.setFont("Helvetica-Bold", 12)
    canv.drawCentredString(40 * mm, y + 340, "Sindicato de transporte")
    canv.drawCentredString(40 * mm, y + 325, '"Rapiditos del Sur"')

    canv.setFont('Helvetica', 6)

    canv.drawRightString(210, y + 305, 'Res. Min.: 555454')

    canv.drawRightString(210, y + 295, 'Of. Potosi: Cll. Abcde # 65')
    canv.drawRightString(210, y + 288, 'Telf. 26222222')

    canv.drawRightString(210, y + 278, 'Of. Sucre: Cll. Abcde # 65')
    canv.drawRightString(210, y + 271, 'Telf. 26222222')

    canv.drawRightString(210, y + 261, 'Of. Camargo: Cll. Abcde # 65')
    canv.drawRightString(210, y + 254, 'Telf. 26222222')

    canv.drawRightString(210, y + 244, 'Horarios de oficina, de Lunes a Sabado de 6:00 a 21:00')
    canv.drawRightString(210, y + 237, 'Domingos de 08:00 a 18:00')
    canv.drawRightString(210, y + 230, 'Horarios de servicio 24 Hrs. de Lunes a Domingo')