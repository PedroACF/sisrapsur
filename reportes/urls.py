from django.urls import path
from . import views


urlpatterns = [
    path('pdf-envio/<int:pk>', views.pdf_envio, name='reporte.envio'),
    path('pdf-boleto/<int:pk>', views.pdf_boleto, name='reporte.boleto'),
    path('personas', views.persona, name='reporte.personas'),
    path('personas-get', views.persona_get, name='reporte.personas.get'),
    path('automoviles', views.automovil, name='reporte.automoviles'),
    path('automovil-get', views.automovil_get, name='reporte.automovil.get'),
    path('ventas', views.ventas, name='reporte.ventas'),
    path('ventas-get', views.ventas_get, name='reporte.ventas.get'),
    path('ventas-lista', views.ventas_lista, name='reporte.ventas.lista'),
    path('ventas-lista-get', views.ventas_lista_get, name='reporte.ventas.lista.get'),
    path('envios', views.envios, name='reporte.envios'),
    path('envios-get', views.envios_get, name='reporte.envios.get'),
    path('facturacion', views.facturacion, name='reporte.facturacion'),
    path('facturacion-get', views.facturacion_get, name='reporte.facturacion.get'),
    path('faltas-atrasos', views.faltas_atrasos, name='reporte.faltas_atrasos'),
    path('faltas-atrasos-get', views.faltas_atrasos_get, name='reporte.faltas_atrasos.get'),
]