import datetime
from django.db import models
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager
from django.core.validators import MinLengthValidator, MinValueValidator


class UsuarioManager(BaseUserManager):
    def create_user(self, usuario, password=None):
        if not usuario:
            raise ValueError('Es necesario definir un nombre de usuario')

        user = self.model(
            usuario=usuario
        )
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, usuario, password):
        user = self.create_user(
            usuario,
            password=password
        )
        user.tipo = 'ADMINISTRADOR'
        user.is_superuser = True
        user.save(using=self._db)
        admin = Administrador(usuario=user)
        admin.save(using=self._db)
        persona = Persona(ci=usuario, nombres='admin', apellidos='admin', direccion='admin#12', telefono='0000000', usuario=user)
        persona.save(using=self._db)
        return user


class Usuario(AbstractBaseUser):
    tipo_opciones = (
        ('ADMINISTRADOR', 'ADMINISTRADOR'),
        ('SECRETARIA', 'SECRETARIA'),
        ('AFILIADO', 'AFILIADO')
    )
    usuario = models.CharField(max_length=50, unique=True, error_messages={'unique': 'El valor ya ha sido tomado'})
    codigo_huella = models.TextField(blank=True, null=True)
    tipo = models.CharField(max_length=15, choices=tipo_opciones, default='AFILIADO')
    activo = models.BooleanField(default=True)
    is_superuser = models.BooleanField(default=False)
    USERNAME_FIELD = 'usuario'
    objects = UsuarioManager()
    #REQUIRED_FIELDS = ['']

    def __str__(self):
        return self.usuario

    def has_perm(self, perm, obj=None):
        "Tiene permisos especificos?"
        return True

    def has_module_perms(self, app_label):
        "Permisos en `app_label`?"
        return True

    def get_turno(self):
        if not hasattr(self, 'secretaria'):
            return None
        asignaciones = self.secretaria.asignaciones.filter(fecha=datetime.date.today(),
                                                           hora_ingreso__lte=datetime.datetime.now(),
                                                           hora_salida__gte=datetime.datetime.now())
        if asignaciones.exists():
            asig = asignaciones[0]
            return asig
        return None

    def get_reemplazo(self):
        asig = self.get_turno()
        if asig is None:
            return None
        try:
            altern = Usuario.objects.get(pk=asig.id_suplente)
        except (Usuario.DoesNotExist, Usuario.MultipleObjectsReturned) as e:
            return None
        return altern


class Persona(models.Model):
    ci = models.CharField(max_length=15, unique=True, validators=[MinLengthValidator(7, 'El minimo de caracteres admitidos es de 7')])
    nombres = models.CharField(max_length=50)
    apellidos = models.CharField(max_length=50)
    direccion = models.CharField(max_length=100)
    telefono = models.IntegerField(validators=[MinValueValidator(6000000, 'El numero de telefono no puede ser tan corto.')])
    usuario = models.OneToOneField(Usuario, on_delete=models.CASCADE)


class Lugar(models.Model):
    nombre = models.CharField(max_length=15, unique=True)
    codigo = models.CharField(max_length=5, unique=True)

    def __str__(self):
        return self.nombre


class Afiliado(models.Model):
    estados = (
        ('HABILITADO', 'HABILITADO'),
        ('SIN ENVIOS', 'SIN ENVIOS'),
        ('LICENCIA', 'LICENCIA'),
        ('SUSPENDIDO', 'SUSPENDIDO'),
    )
    estado = models.CharField(max_length=15, choices=estados, default='HABILITADO')
    usuario = models.OneToOneField(Usuario, on_delete=models.CASCADE)


class Secretaria(models.Model):
    usuario = models.OneToOneField(Usuario, on_delete=models.CASCADE)
    lugar = models.ForeignKey(Lugar, on_delete=models.CASCADE)


class Administrador(models.Model):
    usuario = models.OneToOneField(Usuario, on_delete=models.CASCADE)


