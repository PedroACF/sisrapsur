from django.urls import path
from . import views


urlpatterns = [
    path('', views.home, name='main.home'),
    path('login', views.login_page, name='main.login'),
    path('logout', views.logout_view, name='main.logout'),
    path('checkuser', views.check_user, name='main.checkuser'),
    path('verifyuser', views.verify_user, name='main.verifyuser')
]