from django.urls import path

from . import views

urlpatterns = [
    path('', views.SecretariaList.as_view(), name='secretaria.lista'),
    path('crear', views.secretaria_crear, name='secretaria.crear'),
    path('<int:pk>/editar-datos', views.secretaria_editar_datos, name='secretaria.editar_datos'),
    path('<int:pk>/editar-credenciales', views.secretaria_editar_credenciales, name='secretaria.editar_credenciales'),
    path('<int:pk>/eliminar', views.secretaria_eliminar, name='secretaria.eliminar')
]
