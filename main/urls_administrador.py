from django.urls import path

from . import views

urlpatterns = [
    path('', views.AdministradorList.as_view(), name='administrador.lista'),
    path('crear', views.administrador_crear, name='administrador.crear'),
    path('<int:pk>/editar-datos', views.administrador_editar_datos, name='administrador.editar_datos'),
    path('<int:pk>/editar-credenciales', views.administrador_editar_credenciales, name='administrador.editar_credenciales'),
    path('<int:pk>/eliminar', views.administrador_eliminar, name='administrador.eliminar')
]
