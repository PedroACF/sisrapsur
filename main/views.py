import datetime
from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import render, get_object_or_404, redirect
from django.views.generic import ListView
from django.db import transaction
from django.contrib.auth.decorators import user_passes_test, login_required
from django.contrib.auth import authenticate, login, logout
from .models import Administrador, Secretaria, Usuario
from .forms import UsuarioForm, PersonaForm, SecretariaForm
from django.http import JsonResponse, Http404


#main views
@login_required
def home(request):
    return render(request, 'home.html')


def login_page(request):
    return render(request, 'login.html')


def check_user(request):
    usuario = request.POST["username"]
    user = get_object_or_404(Usuario, usuario=usuario)
    if hasattr(user, 'administrador') or hasattr(user, 'secretaria'):
        return JsonResponse({'success': True, 'codigo_huella': user.codigo_huella})
    return JsonResponse({'success': False})


def verify_user(request):
    usuario = request.POST["username"]
    password = request.POST["password"]
    user = authenticate(usuario=usuario, password=password)
    if user is None:
        user = get_object_or_404(Usuario, usuario=usuario, codigo_huella=password)
    if not user.activo:
        raise Http404
    if hasattr(user, 'secretaria'):
        secre = user.secretaria
        horarios = secre.asignaciones.filter(hora_ingreso__lte=datetime.datetime.now().time(),
                                             hora_salida__gte=datetime.datetime.now().time(),
                                             fecha__exact=datetime.datetime.now())
        if horarios.count() == 0:
            return JsonResponse({'success': False, 'message': 'Fuera de turno'})
        horario = horarios[0]
        if horario.hora_ingreso_reg is None:
            horario.hora_ingreso_reg = datetime.datetime.now()
            horario.save()
    login(request, user)
    return JsonResponse({'success': True})


@login_required
def logout_view(request):
    user = request.user
    asig = user.get_turno()
    if asig is not None and asig.hora_salida_reg is None:
        asig.hora_salida_reg = datetime.datetime.now()
        asig.save()
    logout(request)
    return redirect('/')


# Vistas para administrador
class AdministradorList(LoginRequiredMixin, ListView):
    model = Administrador
    template_name = 'administrador/lista.html'


@login_required
@transaction.atomic
def administrador_crear(request):
    if request.method == 'POST':
        user_form = UsuarioForm(request.POST)
        pers_form = PersonaForm(request.POST)
        if user_form.is_valid() and pers_form.is_valid():
            user = user_form.save(commit=False)
            user.tipo = 'ADMINISTRADOR'
            user.is_superuser = True
            user.save()
            persona = pers_form.save(commit=False)
            persona.usuario = user
            persona.save()
            admin = Administrador(usuario=user)
            admin.save()
            return redirect('administrador.lista')
    else:
        user_form = UsuarioForm()
        pers_form = PersonaForm()
    return render(request, 'administrador/crear.html', {'user_form': user_form, 'pers_form': pers_form})


@login_required
@transaction.atomic
def administrador_editar_datos(request, pk):
    admin = get_object_or_404(Administrador, pk=pk)
    usuario = admin.usuario
    persona = usuario.persona
    if request.method == 'POST':
        form = PersonaForm(request.POST, instance = persona)
        if form.is_valid():
            form.save()
            return redirect('administrador.lista')
    else:
        form = PersonaForm(instance=persona)
    return render(request, 'administrador/editar_datos.html', {'pers_form': form})


@login_required
@transaction.atomic
def administrador_editar_credenciales(request, pk):
    admin = get_object_or_404(Administrador, pk=pk)
    usuario = admin.usuario
    usuario.is_superuser = True
    if request.method == 'POST':
        form = UsuarioForm(request.POST, instance=usuario)
        if form.is_valid():
            form.save()
            return redirect('administrador.lista')
    else:
        form = UsuarioForm(instance = usuario)
    return render(request, 'administrador/editar_credenciales.html', {'user_form': form})


@login_required
@transaction.atomic
def administrador_eliminar(request, pk):
    admin = get_object_or_404(Administrador, pk=pk)
    usuario = admin.usuario
    usuario.delete()
    return JsonResponse({'success': True})


# Vistas para secretarias
class SecretariaList(LoginRequiredMixin, ListView):
    model = Secretaria
    template_name = 'secretaria/lista.html'


@login_required
@transaction.atomic
def secretaria_crear(request):
    if request.method == 'POST':
        user_form = UsuarioForm(request.POST)
        pers_form = PersonaForm(request.POST)
        secre_form = SecretariaForm(request.POST)
        if user_form.is_valid() and pers_form.is_valid() and secre_form.is_valid():
            user = user_form.save(commit=False)
            user.tipo = 'SECRETARIA'
            user.save()
            persona = pers_form.save(commit=False)
            persona.usuario = user
            persona.save()
            secre = secre_form.save(commit=False)
            secre.usuario = user
            secre.save()
            return redirect('secretaria.lista')
    else:
        user_form = UsuarioForm()
        pers_form = PersonaForm()
        secre_form = SecretariaForm()
    return render(request, 'secretaria/crear.html', {'user_form': user_form, 'pers_form': pers_form, 'secre_form': secre_form})


@login_required
@transaction.atomic
def secretaria_editar_datos(request, pk):
    secre = get_object_or_404(Secretaria, pk=pk)
    usuario = secre.usuario
    persona = usuario.persona
    if request.method == 'POST':
        form = PersonaForm(request.POST, instance=persona)
        if form.is_valid():
            form.save()
            return redirect('secretaria.lista')
    else:
        form = PersonaForm(instance = persona)
    return render(request, 'secretaria/editar_datos.html', {'pers_form': form})


@login_required
@transaction.atomic
def secretaria_editar_credenciales(request, pk):
    secre = get_object_or_404(Secretaria, pk=pk)
    usuario = secre.usuario
    if request.method == 'POST':
        user_form = UsuarioForm(request.POST, instance=usuario)
        secre_form = SecretariaForm(request.POST, instance=secre)
        if user_form.is_valid() and secre_form.is_valid():
            user_form.save()
            secre_form.save()
            return redirect('secretaria.lista')
    else:
        user_form = UsuarioForm(instance=usuario)
        secre_form = SecretariaForm(instance=secre)
    return render(request, 'secretaria/editar_credenciales.html', {'user_form': user_form, 'secre_form': secre_form})


@login_required
@transaction.atomic
def secretaria_eliminar(request, pk):
    secre = get_object_or_404(Secretaria, pk=pk)
    usuario = secre.usuario
    usuario.delete()
    return JsonResponse({'success': True})
