# Generated by Django 2.1.2 on 2018-11-19 16:10

from django.conf import settings
import django.core.validators
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Usuario',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('password', models.CharField(max_length=128, verbose_name='password')),
                ('last_login', models.DateTimeField(blank=True, null=True, verbose_name='last login')),
                ('usuario', models.CharField(error_messages={'unique': 'El valor ya ha sido tomado'}, max_length=50, unique=True)),
                ('codigo_huella', models.TextField(blank=True, null=True)),
                ('tipo', models.CharField(choices=[('ADMINISTRADOR', 'ADMINISTRADOR'), ('SECRETARIA', 'SECRETARIA'), ('AFILIADO', 'AFILIADO')], default='AFILIADO', max_length=15)),
                ('activo', models.BooleanField(default=True)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Administrador',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('usuario', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='Afiliado',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('estado', models.CharField(choices=[('HABILITADO', 'HABILITADO'), ('SIN ENVIOS', 'SIN ENVIOS'), ('LICENCIA', 'LICENCIA'), ('SUSPENDIDO', 'SUSPENDIDO')], default='HABILITADO', max_length=15)),
                ('usuario', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='Lugar',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nombre', models.CharField(max_length=15, unique=True)),
                ('codigo', models.CharField(max_length=5, unique=True)),
            ],
        ),
        migrations.CreateModel(
            name='Persona',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('ci', models.CharField(max_length=15, unique=True, validators=[django.core.validators.MinLengthValidator(7, 'El minimo de caracteres admitidos es de 7')])),
                ('nombres', models.CharField(max_length=50)),
                ('apellidos', models.CharField(max_length=50)),
                ('direccion', models.CharField(max_length=100)),
                ('telefono', models.IntegerField(validators=[django.core.validators.MinValueValidator(6000000, 'El numero de telefono no puede ser tan corto.')])),
                ('usuario', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='Secretaria',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('lugar', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='main.Lugar')),
                ('usuario', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
