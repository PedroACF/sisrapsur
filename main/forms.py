from django import forms
from django.forms import ModelForm, ValidationError
from .models import Usuario, Persona, Secretaria


class UsuarioForm(ModelForm):
    password1 = forms.CharField(widget=forms.PasswordInput(attrs={'class': 'form-control'}))
    password2 = forms.CharField(widget=forms.PasswordInput(attrs={'class': 'form-control'}))

    class Meta:
        model = Usuario
        fields = ['usuario', 'codigo_huella']
        widgets = {
            'usuario': forms.TextInput(attrs={'class': 'form-control'}),
            'codigo_huella': forms.HiddenInput()
        }

    def clean_password2(self):
        password1 = self.cleaned_data.get('password1')
        password2 = self.cleaned_data.get('password2')
        if password1 and password2 and password1 != password2:
            raise ValidationError('Passwords no coinciden')
        return password2

    def save(self, commit=True):
        user = super().save(commit=False)
        user.set_password(self.cleaned_data["password1"])
        if commit:
            user.save()
        return user


class PersonaForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if self.instance.pk is not None:
            self.fields['ci'].widget.attrs.update({'readonly': True})

    class Meta:
        model = Persona
        fields = ['ci', 'nombres', 'apellidos', 'direccion', 'telefono']
        widgets = {
            'ci': forms.TextInput(attrs={'class': 'form-control upper only-numbers'}),
            'nombres': forms.TextInput(attrs={'class': 'form-control upper only-letters'}),
            'apellidos': forms.TextInput(attrs={'class': 'form-control upper only-letters'}),
            'direccion': forms.TextInput(attrs={'class': 'form-control upper'}),
            'telefono': forms.NumberInput(attrs={'class': 'form-control upper only-numbers'}),
        }


class SecretariaForm(ModelForm):
    class Meta:
        model = Secretaria
        fields = ['lugar']
        widgets = {
            'lugar': forms.Select(attrs={'class': 'form-control'})
        }