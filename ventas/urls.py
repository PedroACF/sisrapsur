from django.urls import path
from . import views


urlpatterns = [
    path('dosificaciones', views.dosificaciones, name='dosificacion.lista'),
    path('dosificaciones/crear', views.dosificaciones_crear, name='dosificacion.crear'),
    path('dosificaciones/cambiar', views.dosificacion_activar, name='dosificacion.cambiar'),
    path('boleto', views.venta_boleto, name='ventas.boleto'),
    path('factura/<int:pk>', views.factura_mostrar, name='factura.mostrar'),
    path('envios', views.venta_envios, name='ventas.envio'),
    path('envios-recibido/<int:pk>', views.envio_recibido, name='envio.recibido'),
    path('detalle/<int:pk>', views.VentaDetail.as_view(), name='ventas.detalle'),
    path('lista-envios', views.lista_envios, name='lista.envios'),
    path('get-cliente', views.get_cliente, name='ventas.clientes'),
    path('get-usuario', views.get_usuario, name='lista.get_usuario'),
    path('lista-conductores', views.lista_conductores, name='ventas.lista_conductores'),
    path('lista-conductores/orden', views.lista_orden, name='lista.orden'),
    path('lista-conductores/cargar', views.lista_cargar, name='lista.cargar'),
    path('lista-conductores/agregar', views.lista_agregar, name='lista.agregar'),
    path('lista-conductores/eliminar/<int:pk>', views.lista_eliminar, name='lista.delete')
]

