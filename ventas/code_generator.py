import math


class CodeGenerator:
    @staticmethod
    def generate(autorizacion, factura, nitci, fecha, monto, llave):
        monto = math.ceil(monto)
        numeroFactura = CodeGenerator.add_verhoeff_digit(factura, 2)
        nit_ci = CodeGenerator.add_verhoeff_digit(nitci, 2)
        fecha_t = CodeGenerator.add_verhoeff_digit(fecha, 2)
        monto_t = CodeGenerator.add_verhoeff_digit(monto, 2)
        suma = int(numeroFactura) + int(nit_ci) + int(fecha_t) + int(monto_t)
        suma_verhoeff = CodeGenerator.add_verhoeff_digit(suma, 5)

        five_digits_verhoeff = suma_verhoeff[len(suma_verhoeff)-5:]
        numbers = [(int(five_digits_verhoeff[i])+1) for i in range(0, len(five_digits_verhoeff))]
        string1 = llave[:numbers[0]]
        string2 = llave[numbers[0]: numbers[0]+numbers[1]]
        string3 = llave[numbers[0]+numbers[1]: numbers[0]+numbers[1]+numbers[2]]
        string4 = llave[numbers[0]+numbers[1]+numbers[2]:  numbers[0]+numbers[1]+numbers[2]+numbers[3]]
        string5 = llave[numbers[0] + numbers[1] + numbers[2] + numbers[3]:  numbers[0] + numbers[1] + numbers[2] + numbers[3] + numbers[4]]
        aut_key = str(autorizacion) + string1
        fact_key = str(numeroFactura) + string2
        nit_ci_key = nit_ci + string3
        fecha_key = fecha_t + string4
        monto_key = monto_t + string5

        string_key = aut_key + fact_key + nit_ci_key + fecha_key + monto_key
        clave_enc = llave + five_digits_verhoeff
        alleged = AllegedRC4.encrypt_message_rc4_unscripted(string_key, clave_enc)

        total = sp1 = sp2 = sp3 = sp4 = sp5 = 0
        tmp = 1
        for c in alleged:
            total += ord(c)
            if tmp == 1:
                sp1 += ord(c)
            if tmp == 2:
                sp2 += ord(c)
            if tmp == 3:
                sp3 += ord(c)
            if tmp == 4:
                sp4 += ord(c)
            if tmp == 5:
                sp5 += ord(c)
            tmp = (tmp+1) if tmp < 5 else 1
        tmp1 = total * sp1 / numbers[0]
        tmp2 = total * sp2 / numbers[1]
        tmp3 = total * sp3 / numbers[2]
        tmp4 = total * sp4 / numbers[3]
        tmp5 = total * sp5 / numbers[4]
        suma = tmp1 + tmp2 + tmp3 + tmp4 + tmp5
        base64 = Base64SIN.convert(suma)

        return AllegedRC4.encrypt_message_rc4(base64, llave+five_digits_verhoeff)

    @staticmethod
    def add_verhoeff_digit(value, max):
        verhoeff = Verhoeff()
        result = str(value)
        for i in range(0, max):
            result = result + str(verhoeff.generate_verhoeff(int(result)))
        return result


class Verhoeff:
    #multip
    d = [
        [0, 1, 2, 3, 4, 5, 6, 7, 8, 9],
        [1, 2, 3, 4, 0, 6, 7, 8, 9, 5],
        [2, 3, 4, 0, 1, 7, 8, 9, 5, 6],
        [3, 4, 0, 1, 2, 8, 9, 5, 6, 7],
        [4, 0, 1, 2, 3, 9, 5, 6, 7, 8],
        [5, 9, 8, 7, 6, 0, 4, 3, 2, 1],
        [6, 5, 9, 8, 7, 1, 0, 4, 3, 2],
        [7, 6, 5, 9, 8, 2, 1, 0, 4, 3],
        [8, 7, 6, 5, 9, 3, 2, 1, 0, 4],
        [9, 8, 7, 6, 5, 4, 3, 2, 1, 0]
    ]

    #permut
    p = [
        [0, 1, 2, 3, 4, 5, 6, 7, 8, 9],
        [1, 5, 7, 6, 2, 8, 3, 0, 9, 4],
        [5, 8, 0, 3, 7, 9, 6, 1, 4, 2],
        [8, 9, 1, 6, 0, 4, 3, 5, 2, 7],
        [9, 4, 5, 3, 1, 2, 6, 8, 7, 0],
        [4, 2, 8, 6, 5, 7, 3, 9, 0, 1],
        [2, 7, 9, 3, 8, 0, 6, 4, 1, 5],
        [7, 0, 4, 6, 9, 1, 3, 2, 5, 8]
    ]

    #inverse
    inv = [0, 4, 3, 2, 1, 5, 6, 7, 8, 9]

    def generate_verhoeff(self, num):
        c = 0
        myArray = self.string_to_reversed_int_array(num)
        for i in range(0, len(myArray)):
            c = self.d[c][self.p[((i + 1) % 8)][int(myArray[i])]]
        return str(self.inv[c])

    def validate_verhoeff(self, num):
        c = 0
        myArray = self.string_to_reversed_int_array(num)
        for i in range(0, len(myArray)):
            c = self.d[c][self.p[(i % 8)][myArray[i]]]
        return c == 0

    def string_to_reversed_int_array(self, num):
        return list(reversed(str(num)))


class Base64SIN:

    @staticmethod
    def convert(value):
        dictionary = [
            "0", "1", "2", "3", "4", "5", "6", "7",
            "8", "9", "A", "B", "C", "D", "E", "F",
            "G", "H", "I", "J", "K", "L", "M", "N",
            "O", "P", "Q", "R", "S", "T", "U", "V",
            "W", "X", "Y", "Z", "a", "b", "c", "d",
            "e", "f", "g", "h", "i", "j", "k", "l",
            "m", "n", "o", "p", "q", "r", "s", "t",
            "u", "v", "w", "x", "y", "z", "+", "/"
        ]
        quotient = 1
        word = ''
        while quotient > 0:
            quotient = value // 64
            remainder = value % 64
            word = dictionary[int(remainder)] + word
            value = quotient
        return word


class AllegedRC4:
    @staticmethod
    def encrypt_message_rc4(message, key):
        state = [i for i in range(0, 256)]
        x = y = index1 = index2 = 0
        message_encryption = ""
        for i in range(0, 256):
            index2 = (ord(key[index1]) + state[i] + index2) % 256
            aux = state[i]
            state[i] = state[index2]
            state[index2] = aux
            index1 = (index1+1) % len(key)
        for i in range(0, len(message)):
            x = (x + 1) % 256
            y = (state[x] + y) % 256
            aux = state[x]
            state[x] = state[y]
            state[y] = aux
            nmen = ord(message[i]) ^ state[(state[x]+state[y]) % 256]
            nmenHex = hex(nmen)[2:].upper()
            message_encryption = message_encryption + "-" + (("0"+nmenHex) if len(nmenHex) == 1 else nmenHex)
        return message_encryption[1:]

    @staticmethod
    def encrypt_message_rc4_unscripted(message, key):
        result = AllegedRC4.encrypt_message_rc4(message, key)
        return result.replace("-", "")