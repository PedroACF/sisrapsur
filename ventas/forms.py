import datetime
from .models import Cliente, Venta, Boleto, Envio, Lista, Dosificacion
from main.models import Persona
from django import forms
from django.db.models import Max
from django.forms import ModelForm, ValidationError
from django.contrib.auth import authenticate
from main.models import Usuario


class ClienteForm(ModelForm):
    class Meta:
        model = Cliente
        fields = ['ci_nit', 'nombre', 'cel']
        widgets = {
            'ci_nit': forms.TextInput(attrs={'class': 'form-control only-numbers'}),
            'nombre': forms.TextInput(attrs={'class': 'form-control only-letters upper'}),
            'cel': forms.NumberInput(attrs={'class': 'form-control only-numbers'}),
        }

    def clean_ci_nit(self):
        ci = self.cleaned_data.get('ci_nit')
        try:
            persona = Persona.objects.get(ci=ci)
        except (Persona.DoesNotExist, Persona.MultipleObjectsReturned) as e:
            persona = None
        if persona is not None:
            raise ValidationError('CI pertenece a un usuario del sistema')
        return ci


class VentaForm(ModelForm):
    class Meta:
        model = Venta
        fields = ['origen', 'destino', 'total']
        widgets = {
            'origen': forms.TextInput(attrs={'class': 'form-control', 'readonly': True}),
            'destino': forms.Select(attrs={'class': 'form-control'}),
            'total': forms.NumberInput(attrs={'class': 'form-control'}),
        }

    def clean_destino(self):
        origen = self.cleaned_data.get('origen')
        destino = self.cleaned_data.get('destino')
        try:
            item = Lista.objects.filter(origen=origen,
                                         destino=destino,
                                         activo=True,
                                         fecha=datetime.date.today()).order_by('orden')[0:1].get()
            return destino
        except Lista.DoesNotExist:
            raise ValidationError('No existe ningun conductor con el destino seleccionado')

    def save(self, commit=True):
        origen = self.cleaned_data.get('origen')
        destino = self.cleaned_data.get('destino')
        venta = super().save(commit=False)
        item = Lista.objects.filter(origen=origen,
                                    destino=destino,
                                    activo=True,
                                    fecha=datetime.date.today()).order_by('orden')[0:1].get()
        venta.lista = item
        if commit:
            venta.save()
        return venta


class BoletoForm(ModelForm):
    class Meta:
        model = Boleto
        fields = ['plazas']
        widgets = {
            'plazas': forms.NumberInput(attrs={'class': 'form-control'})
        }


class EnvioForm(ModelForm):
    class Meta:
        model = Envio
        fields = ['ci_consignatario', 'nombre_consignatario', 'telefono_consignatario', 'tipo', 'cantidad', 'detalle', 'tipo_pago']
        widgets = {
            'ci_consignatario': forms.TextInput(attrs={'class': 'form-control'}),
            'nombre_consignatario': forms.TextInput(attrs={'class': 'form-control upper'}),
            'telefono_consignatario': forms.NumberInput(attrs={'class': 'form-control'}),
            'cantidad': forms.NumberInput(attrs={'class': 'form-control'}),
            'tipo': forms.Select(attrs={'class': 'form-control'}),
            'detalle': forms.Textarea(attrs={'class': 'form-control upper', 'rows': 2}),
            'tipo_pago': forms.RadioSelect(attrs={'class': 'icheck'})
        }


class RegistroAfiliadoForm(forms.Form):
    usuario = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'}))
    huella = forms.CharField(required=False, widget=forms.HiddenInput(attrs={'disabled': True}))
    clave = forms.CharField(required=False, widget=forms.PasswordInput(attrs={'class': 'form-control'}))
    destino = forms.CharField(widget=forms.Select(attrs={'class': 'form-control'}))

    def clean_clave(self):
        username = self.cleaned_data.get('usuario')
        clave = self.cleaned_data.get('clave')
        huella = self.cleaned_data.get('huella')
        user = self.get_user(username, clave, huella)
        if user is None:
            raise ValidationError('Credenciales incorrectas')
        if not hasattr(user, 'afiliado'):
            raise ValidationError('Usuario no es afiliado')
        if user.afiliado.estado == 'LICENCIA' or user.afiliado.estado == 'SUSPENDIDO':
            raise ValidationError('Usted no esta habilitado para ingresar a la lista')
        if not hasattr(user.afiliado, 'automovil'):
            raise ValidationError('El afiliado no tiene un vehiculo asignado')
        item = Lista.objects.filter(fecha=datetime.date.today(), afiliado=user.afiliado,
                                    automovil=user.afiliado.automovil, activo=True)
        if item.exists():
            raise ValidationError('El afiliado esta registrado en la lista')
        return clave

    def get_user(self, username, clave, huella):
        user = authenticate(username=username, password=clave)
        if user is None:
            try:
                user = Usuario.objects.get(usuario=username, codigo_huella=huella)
                return user
            except (Usuario.DoesNotExist, Usuario.MultipleObjectsReturned) as e:
                return None
        return user

    def save(self, commit=True):
        item = Lista()
        username = self.cleaned_data.get('usuario')
        huella = self.cleaned_data.get('huella')
        clave = self.cleaned_data.get('clave')
        destino = self.cleaned_data.get('destino')
        user = self.get_user(username, clave, huella)
        item.afiliado = user.afiliado
        item.automovil = user.afiliado.automovil
        item.destino = destino
        max = Lista.objects.filter(fecha=datetime.date.today()).aggregate(Max('orden'))
        if max['orden__max'] is None:
            item.orden = 1
        else:
            item.orden = max['orden__max'] + 1
        if commit:
            item.save()
        return item


class DosificacionForm(ModelForm):
    fecha_limite2 = forms.DateField(widget=forms.DateInput(format="%m/%d/%Y", attrs={'class': 'form-control datepicker'}))
    factura_inicio2 = forms.IntegerField(widget=forms.NumberInput(attrs={'class': 'form-control only-numbers'}))
    factura_fin2 = forms.IntegerField(widget=forms.NumberInput(attrs={'class': 'form-control only-numbers'}))
    codigo_autorizacion2 = forms.IntegerField(widget=forms.NumberInput(attrs={'class': 'form-control only-numbers'}))
    llave2 = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'}))

    class Meta:
        model = Dosificacion
        fields = ['lugar', 'fecha_limite', 'factura_inicio', 'factura_fin', 'codigo_autorizacion', 'llave']
        widgets = {
            'lugar': forms.Select(attrs={'class': 'form-control'}),
            'fecha_limite': forms.DateInput(format="%m/%d/%Y", attrs={'class': 'form-control datepicker'}),
            'factura_inicio': forms.NumberInput(attrs={'class': 'form-control only-numbers'}),
            'factura_fin': forms.NumberInput(attrs={'class': 'form-control only-numbers'}),
            'codigo_autorizacion': forms.NumberInput(attrs={'class': 'form-control only-numbers'}),
            'llave': forms.TextInput(attrs={'class': 'form-control'})
        }

    def clean_fecha_limite2(self):
        fechalimite2 = self.cleaned_data.get('fecha_limite2')
        fechalimite = self.cleaned_data.get('fecha_limite')
        if fechalimite != fechalimite2:
            raise ValidationError('Debe ser igual a la fecha limite')
        return fechalimite2

    def clean_factura_inicio2(self):
        factura_inicio = self.cleaned_data.get('factura_inicio')
        factura_inicio2 = self.cleaned_data.get('factura_inicio2')
        if factura_inicio != factura_inicio2:
            raise ValidationError('Debe ser igual a la factura inicio')
        return factura_inicio2

    def clean_factura_fin2(self):
        factura_inicio = self.cleaned_data.get('factura_inicio')
        factura_fin = self.cleaned_data.get('factura_fin')
        factura_fin2 = self.cleaned_data.get('factura_fin2')
        if factura_fin != factura_fin2:
            raise ValidationError('Debe ser igual a la factura fin')
        if factura_fin2 <= factura_inicio:
            raise ValidationError('Debe ser mayor a la factura inicio')
        return factura_fin2

    def clean_codigo_autorizacion2(self):
        codigo_autorizacion = self.cleaned_data.get('codigo_autorizacion')
        codigo_autorizacion2 = self.cleaned_data.get('codigo_autorizacion2')
        print(codigo_autorizacion)
        print(codigo_autorizacion2)
        if codigo_autorizacion != codigo_autorizacion2:
            raise ValidationError('Debe ser igual al codigo de autorizacion')
        return codigo_autorizacion2

    def clean_llave2(self):
        llave = self.cleaned_data.get('llave')
        llave2 = self.cleaned_data.get('llave2')
        if llave != llave2:
            raise ValidationError('Debe ser igual al codigo de llave de dosificacion')
        return llave2

