import datetime

from django.db import transaction
from django.db.models import Q, Max
from django.http import HttpResponse
from django.http import JsonResponse
from django.shortcuts import render, get_object_or_404, redirect
from django.views.generic import DetailView
from reportlab.graphics import renderPDF
from reportlab.graphics.barcode import qr
from reportlab.graphics.shapes import Drawing
from reportlab.lib.styles import getSampleStyleSheet
from reportlab.lib.units import mm
from reportlab.pdfbase import pdfmetrics
from reportlab.pdfbase.ttfonts import TTFont
from reportlab.pdfgen import canvas
from reportlab.platypus.paragraph import Paragraph

from main.models import Lugar, Usuario
from .code_generator import CodeGenerator
from .forms import ClienteForm, VentaForm, BoletoForm, EnvioForm, RegistroAfiliadoForm, DosificacionForm
from .models import Lista, Cliente, Envio, Venta, Dosificacion, Factura


#from reportlab.lib.pagesizes import letter, landscape


def lista_conductores(request):
    lugar_actual = request.user.secretaria.lugar.nombre
    conductores = Lista.objects.filter(fecha=datetime.date.today(), origen=lugar_actual).order_by('orden', 'hora_entrada')
    return render(request, 'lista_conductores.html', {'conductores': conductores})


@transaction.atomic
def lista_orden(request):
    pks = request.POST.getlist('datos[]')
    pkplus = pks[0]
    pkminus = pks[1]
    item = get_object_or_404(Lista, pk=pkplus)
    item.orden = item.orden+1
    item.save()
    item = get_object_or_404(Lista, pk=pkminus)
    item.orden = item.orden - 1
    item.save()
    return JsonResponse({'success': True})


def lista_cargar(request):
    pk = request.POST["pk"]
    item = get_object_or_404(Lista, pk=pk)
    item.activo = False
    item.hora_salida = datetime.datetime.now().time()
    item.save()
    return JsonResponse({'success': True})


def lista_agregar(request):
    lugar_actual = request.user.secretaria.lugar.nombre
    choices = [(lugar.nombre, lugar.nombre) for lugar in Lugar.objects.exclude(nombre=lugar_actual)]
    form = RegistroAfiliadoForm()
    form.fields['destino'].widget.choices = choices
    if request.method == 'POST':
        form = RegistroAfiliadoForm(request.POST)
        form.fields['destino'].widget.choices = choices
        if form.is_valid():
            item = form.save(commit=False)
            item.origen = lugar_actual
            item.save()
            return redirect('ventas.lista_conductores')
    return render(request, 'lista_agregar.html', {'form': form})


def lista_eliminar(request, pk):
    try:
        item = Lista.objects.get(pk=pk)
        item.delete()
    except Lista.DoesNotExist:
        print('No existe item')
    return redirect('ventas.lista_conductores')



def get_cliente(request):
    ci = request.GET.get('ci', '')
    cliente = get_object_or_404(Cliente, ci_nit=ci)
    return JsonResponse({'ci': cliente.ci_nit, 'nombre': cliente.nombre, 'cel': cliente.cel})


def get_usuario(request):
    usuario = request.GET.get('usuario', '')
    try:
        usuario = Usuario.objects.get(usuario=usuario)
        return JsonResponse({'success': True, 'codigo_huella': usuario.codigo_huella})
    except Usuario.DoesNotExist:
        return JsonResponse({'success': False, 'message': 'Usuario no existe'})


@transaction.atomic
def venta_boleto(request):
    lugar_actual = request.user.secretaria.lugar.nombre
    choices = [(lugar.nombre, lugar.nombre) for lugar in Lugar.objects.exclude(nombre=lugar_actual)]
    if request.method == 'POST':
        venta_form = VentaForm(request.POST)
        boleto_form = BoletoForm(request.POST)
        venta_form.fields['destino'].widget.choices = choices
        ci = request.POST.get('ci_nit', '')
        try:
            cliente = Cliente.objects.get(ci_nit=ci)
            cliente_form = ClienteForm(request.POST, instance=cliente)
        except (Cliente.DoesNotExist, Cliente.MultipleObjectsReturned) as e:
            cliente_form = ClienteForm(request.POST)
        if cliente_form.is_valid() and venta_form.is_valid() and boleto_form.is_valid():
            cliente = cliente_form.save()
            venta = venta_form.save(commit=False)
            venta.cliente = cliente
            venta.responsable = request.user.secretaria
            asig = request.user.get_turno()
            if asig is not None:
                venta.responsable_sup = asig.id_suplente
            venta.save()
            boleto = boleto_form.save(commit=False)
            boleto.venta = venta
            boleto.save()
            return redirect('ventas.detalle', pk=venta.id)
    else:
        cliente_form = ClienteForm()
        venta_form = VentaForm(initial={'origen': lugar_actual})
        venta_form.fields['destino'].widget.choices = choices
        boleto_form = BoletoForm()
    return render(request, 'venta_boletos.html', {'cliente_form': cliente_form, 'venta_form': venta_form, 'boleto_form':boleto_form})


@transaction.atomic
def venta_envios(request):
    lugar_actual = request.user.secretaria.lugar.nombre
    choices = [(lugar.nombre, lugar.nombre) for lugar in Lugar.objects.exclude(nombre=lugar_actual)]
    if request.method == 'POST':
        venta_form = VentaForm(request.POST)
        venta_form.fields['destino'].widget.choices = choices
        envio_form = EnvioForm(request.POST)
        ci = request.POST.get('ci_nit', '')
        try:
            cliente = Cliente.objects.get(ci_nit=ci)
            cliente_form = ClienteForm(request.POST, instance=cliente)
        except (Cliente.DoesNotExist, Cliente.MultipleObjectsReturned) as e:
            cliente_form = ClienteForm(request.POST)
        if cliente_form.is_valid() and venta_form.is_valid() and envio_form.is_valid():
            cliente = cliente_form.save()
            venta = venta_form.save(commit=False)
            venta.tipo = 'ENVIO'
            venta.cliente = cliente
            venta.responsable = request.user.secretaria
            asig = request.user.get_turno()
            if asig is not None:
                venta.responsable_sup = asig.id_suplente
            venta.save()
            envio = envio_form.save(commit=False)
            envio.venta = venta
            envio.nro_guia = get_guia(request.user.secretaria.lugar.codigo)
            envio.save()
            return redirect('ventas.detalle', pk=venta.id)
    else:
        cliente_form = ClienteForm()
        venta_form = VentaForm(initial={'origen': lugar_actual})
        venta_form.fields['destino'].widget.choices = choices
        envio_form = EnvioForm()
    return render(request, 'venta_envios.html',
                  {'cliente_form': cliente_form, 'venta_form': venta_form, 'envio_form': envio_form})


def lista_envios(request):
    lista = []
    ci = ''
    if request.method == 'POST':
        ci = request.POST.get('ci', '')
        guia = request.POST.get('ci', '')
        nombre = request.POST.get('ci', '')
        lista = Envio.objects.filter(Q(nombre_consignatario__icontains=ci) |
                                     Q(ci_consignatario=ci) | Q(nro_guia__iexact=guia)).order_by('venta__fecha_hora')
    return render(request, 'lista_envios.html', {'lista': lista, 'ci': ci})


class VentaDetail(DetailView):
    model = Venta
    template_name = 'detalle_venta.html'


def envio_recibido(request, pk):
    envio = get_object_or_404(Envio, pk=pk)
    envio.recibido = True
    envio.save()
    return redirect('ventas.detalle', pk=envio.venta.id)


def dosificaciones(request):
    dosificaciones = Dosificacion.objects.order_by('lugar_id', '-fecha_limite')
    return render(request, 'dosificaciones.html', {'dosificaciones': dosificaciones})


@transaction.atomic
def dosificaciones_crear(request):
    form = DosificacionForm()
    if request.method == 'POST':
        form = DosificacionForm(request.POST)
        if form.is_valid():
            dosi = form.save(commit=False)
            Dosificacion.objects.filter(lugar=dosi.lugar).update(activo=False)
            dosi.save()
            return redirect('dosificacion.lista')
    return render(request, 'dosificaciones_crear.html', {'form': form})


def dosificacion_activar(request):
    vigente = request.GET.get('vigente', 'n')
    pk = request.GET.get('pk', 0)
    dosi = get_object_or_404(Dosificacion, pk=pk)
    if vigente == 'n':
        dosi.activo = False
        dosi.save()
    else:
        Dosificacion.objects.filter(lugar=dosi.lugar).update(activo=False)
        dosi.activo = True
        dosi.save()
    return JsonResponse({'success': True})


def factura_mostrar(request, pk):
    venta = get_object_or_404(Venta, pk=pk)
    lugar_actual = request.user.secretaria.lugar
    try:
        dosificacion = Dosificacion.objects.filter(activo=True, lugar=lugar_actual)[0:1].get()
    except Dosificacion.DoesNotExist:
        return render(request, 'sin_dosificacion.html')
    if hasattr(venta, 'factura'):
        factura = venta.factura
    else:
        factura = Factura(ci_nit=venta.cliente.ci_nit, nombre=venta.cliente.nombre, importe=venta.total, dosificacion=dosificacion, venta=venta)
        max = dosificacion.factura_set.aggregate(Max('numero'))
        if max['numero__max'] is None:
            factura.numero = dosificacion.factura_inicio
        else:
            factura.numero = max['numero__max'] + 1
        factura.save()

    control_code = CodeGenerator.generate(factura.dosificacion.codigo_autorizacion,
                                          factura.numero,
                                          int(factura.venta.cliente.ci_nit),
                                          int(factura.fecha_hora.strftime("%Y%m%d")),
                                          factura.importe,
                                          factura.dosificacion.llave
                                          )

    qr_string = "%d|%d|%d|%s|%f|%f|%s|%d|%d|%d|%d|%d" % (
        12333,
        factura.numero,
        int(factura.dosificacion.codigo_autorizacion),
        factura.fecha_hora.strftime("%d/%m/%Y"),
        factura.importe,
        factura.importe*0.13,
        control_code,
        int(factura.ci_nit),
        0, 0, 0, 0
    )

    response = HttpResponse(content_type='application/pdf')
    response['Content-Disposition'] = 'filename = "boleto.pdf"'
    # Create the PDF object, using the response object as its "file."
    pdfmetrics.registerFont(TTFont('Tahoma', 'Tahoma.ttf'))
    pdfmetrics.registerFont(TTFont('Tahoma-Bold', 'TahomaBd.ttf'))
    p = canvas.Canvas(response, pagesize=(80 * mm, 130 * mm))
    p.setTitle("Default")
    p.setFont("Tahoma-Bold", 11)

    p.drawCentredString(40 * mm, 350, "Sindicato de transporte")
    p.drawCentredString(40 * mm, 338, '"Rapiditos del Sur"')
    p.setFont("Tahoma", 7)
    p.drawCentredString(40 * mm, 328, 'De: Reynaldo Talavera Campos')

    p.drawCentredString(40 * mm, 314, 'SUCURSAL No. 1')
    p.drawCentredString(40 * mm, 305, 'CALLE ABC No. 12')
    p.drawCentredString(40 * mm, 296, 'TELEFONO 2621212')
    p.drawCentredString(40 * mm, 287, '%s - BOLIVIA' % factura.dosificacion.lugar.nombre)

    p.setFont("Tahoma-Bold", 10)
    p.drawCentredString(40*mm, 273, 'FACTURA')

    #factura
    p.setFont("Tahoma", 9)
    line = p.beginPath()
    line.moveTo(15, 270)
    line.lineTo(210, 270)
    p.drawPath(line)
    p.setFont("Tahoma", 9)
    p.drawCentredString((80*mm)/2, 260, "NIT: %d" % 4007424019)
    p.drawCentredString((80 * mm) / 2, 250, "No. FACTURA: %d" % factura.numero)
    p.drawCentredString((80 * mm) / 2, 240, "No. AUTORIZACION: %d" % factura.dosificacion.codigo_autorizacion)
    #datos de factura
    line = p.beginPath()
    line.moveTo(15, 236)
    line.lineTo(210, 236)
    p.drawPath(line)
    p.setFont("Tahoma", 8)
    p.drawString(15, 220, "Fecha: %s" % (factura.fecha_hora.strftime("%d/%m/%Y")))
    p.drawRightString(210, 220, "Hora: %s" % (factura.fecha_hora.strftime("%H:%M")))
    p.drawString(15, 210, "NIT/CI: %s" % factura.ci_nit)
    p.drawString(15, 200, "Señor(es): %s" % factura.nombre)
    #detalle
    p.setFont("Tahoma-Bold", 8)
    p.drawString(15, 185, 'DETALLE')
    p.drawRightString(210, 185, 'SUBTOTAL')
    p.setFont("Tahoma", 8)
    if hasattr(factura.venta, "boleto"):
        p.drawString(15, 175, 'BOLETO')
    else:
        p.drawString(15, 175, "ENVIO")
        p.drawString(15, 165, factura.venta.envio.detalle)
    p.drawRightString(210, 175, "{:.2f}".format(factura.venta.total))

    p.setFont("Tahoma-Bold", 9)
    p.drawRightString(210, 147, "TOTAL BS. {:.2f}".format(factura.importe))
    p.setFont('Tahoma', 8)
    style = getSampleStyleSheet()['Normal']
    style.fontName = 'Tahoma'
    style.fontSize = 8
    style.leading = 10
    par = Paragraph("SON: %s" % convert_to_literal(factura.importe), style)
    wt, ht = par.wrapOn(p, 70*mm, 130*mm)
    par.drawOn(p, 15, 147 - ht)
    #p.drawString(15, 130, )

    line = p.beginPath()
    line.moveTo(15, 125)
    line.lineTo(210, 125)
    p.drawPath(line)

    p.setFont("Tahoma-Bold", 8)
    p.drawString(15, 115, 'CODIGO DE CONTROL: ')
    p.drawString(15, 105, 'FECHA LIMITE DE EMISION:')
    p.setFont("Tahoma", 8)
    p.drawString(110, 115, control_code)
    p.drawString(130, 105, factura.dosificacion.fecha_limite.strftime("%d/%m/%Y"))

    #qr y leyenda
    qr_code = qr.QrCodeWidget(qr_string)
    qr_code.barLevel = 'M'
    bounds = qr_code.getBounds()
    width = bounds[2] - bounds[0]
    height = bounds[3] - bounds[1]
    d = Drawing(45, 45, transform=[80. / width, 0, 0, 80. / height, 0, 0])
    d.add(qr_code)
    renderPDF.draw(d, p, 70, 25)
    p.setFont("Tahoma", 6)
    p.drawString(35, 23, '"ESTA FACTURA CONTRIBUYE AL DESARROLLO DEL PAIS."')
    p.drawString(20, 15, '"EL USO ILICITO DE ESTA SERA SANCIONADO DE ACUERDO A LEY"')

    p.showPage()
    p.save()
    return response


def print_default():
    response = HttpResponse(content_type='application/pdf')
    response['Content-Disposition'] = 'filename = "boleto.pdf"'
    # Create the PDF object, using the response object as its "file."
    p = canvas.Canvas(response, pagesize=(80 * mm, 130 * mm))
    p.setTitle("Default")
    p.setFont("Helvetica-Bold", 14)

    p.drawString(40, 340, "Sindicato de transporte")
    p.drawString(52, 325, '"Rapiditos del Sur"')

    p.showPage()
    p.save()
    return response


def convert_to_literal(quantity):
    number = quantity * 100
    quantity = int(number // 100)
    decimal = int(number) % 100

    quantities = {900: 'NOVECIENTOS', 800: 'OCHOCIENTOS', 700: 'SETECIENTOS', 600: 'SEISCIENTOS', 500: 'QUINIENTOS',
                  400: 'CUATROCIENTOS', 300: 'TRESCIENTOS', 200: 'DOSCIENTOS', 100: 'CIENTO', 90: 'NOVENTA',
                  80: 'OCHENTA', 70: 'SETENTA', 60: 'SESENTA', 50: 'CINCUENTA', 40: 'CUARENTA', 30: 'TREINTA',
                  29: 'VEINTINUEVE', 28: 'VEINTIOCHO', 27: 'VEINTISIETE', 26: 'VEINTISEIS', 25: 'VEINTICINCO',
                  24: 'VEINTICUATRO', 23: 'VEINTITRES', 22: 'VEINTIDOS', 21: 'VEINTIUNO', 20: 'VEINTE',
                  19: 'DIECINUEVE', 18: 'DIECIOCHO', 17: 'DIECISIETE', 16: 'DIECISEIS', 15: 'QUINCE', 14: 'CATORCE',
                  13: 'TRECE', 12: 'DOCE', 11: 'ONCE', 10: 'DIEZ', 9: 'NUEVE', 8: 'OCHO', 7: 'SIETE', 6: 'SEIS',
                  5: 'CINCO', 4: 'CUATRO', 3: 'TRES', 2: 'DOS', 1: 'UN'}
    literal = ""

    for qy in quantities:
        current_qy = qy
        if (current_qy*1000) <= quantity:
            if current_qy == 100 and quantity//1000 == current_qy:
                literal += 'CIEN '
            else:
                literal += (quantities[current_qy] + ' ')
            quantity -= (current_qy*1000)
            if 30 <= current_qy <= 90 and quantity > 0:
                literal += 'Y '

    if len(literal) > 0:
        literal += 'MIL '
    #quantities[1] = 'UNO'

    for qy in quantities:
        current_qy = qy
        if current_qy <= quantity:
            if current_qy == 100 and quantity == current_qy:
                literal += 'CIEN '
            else:
                literal += (quantities[current_qy] + ' ')
            quantity -= current_qy
            if 30 <= current_qy <= 90 and quantity > 0:
                literal += 'Y '
    return "%s %02d/100 Boliviano(s)" % (literal.strip().capitalize(), decimal)


def get_guia(codigo_lugar):
    last = Envio.objects.filter(nro_guia__startswith=codigo_lugar).aggregate(Max('nro_guia'))
    if last['nro_guia__max'] is None:
        return '%s-%d' % (codigo_lugar, 1)
    else:
        last_code = last['nro_guia__max']
        last_number = int(last_code[last_code.find('-')+1 if last_code.find('-') else 0:])
        return '%s-%d' % (codigo_lugar, last_number+1)

