import datetime
from django.db import models
from main.models import Afiliado, Lugar, Secretaria
from afiliados.models import Automovil


class Lista(models.Model):
    fecha = models.DateField(default=datetime.date.today)
    automovil = models.ForeignKey(Automovil, on_delete=models.CASCADE)
    afiliado = models.ForeignKey(Afiliado, on_delete=models.CASCADE)
    hora_entrada = models.TimeField(default=datetime.datetime.now)
    hora_salida = models.TimeField(null=True, blank=True)
    orden = models.IntegerField(default=1)
    activo = models.BooleanField(default=True)
    origen = models.CharField(max_length=15)
    destino = models.CharField(max_length=15)


class Cliente(models.Model):
    ci_nit = models.CharField(max_length=10, unique=True)
    nombre = models.CharField(max_length=50)
    cel = models.IntegerField()


class Venta(models.Model):
    tipos = (
        ('ENVIO', 'ENVIO'),
        ('BOLETO', 'BOLETO')
    )

    fecha_hora = models.DateTimeField(default=datetime.datetime.now)
    tipo = models.CharField(choices=tipos, default='BOLETO', max_length=15)
    origen = models.CharField(max_length=15)
    destino = models.CharField(max_length=15)
    cliente = models.ForeignKey(Cliente, on_delete=models.CASCADE)
    total = models.FloatField(default=0)
    lista = models.ForeignKey(Lista, on_delete=models.CASCADE)
    responsable = models.ForeignKey(Secretaria, on_delete=models.CASCADE)
    responsable_sup = models.IntegerField(default=0, blank=True, null=True)


class Envio(models.Model):
    tipos = (
        ('GIRO', 'GIRO'),
        ('SOBRE', 'SOBRE'),
        ('PAQUETE', 'PAQUETE'),
        ('CAJA', 'CAJA'),
        ('BOLSA', 'BOLSA'),
        ('OTROS', 'OTROS'),
    )

    pagos = (
        ('PAGADO', 'PAGADO'),
        ('POR PAGAR', 'POR PAGAR')
    )

    nro_guia = models.CharField(max_length=10)
    ci_consignatario = models.CharField(max_length=12)
    nombre_consignatario = models.CharField(max_length=50)
    telefono_consignatario = models.CharField(max_length=12)
    tipo = models.CharField(max_length=10, choices=tipos, default='OTROS')
    cantidad = models.IntegerField(default=1)
    detalle = models.TextField()
    recibido = models.BooleanField(default=False)
    tipo_pago = models.CharField(max_length=10, choices=pagos, default='PAGADO')
    venta = models.OneToOneField(Venta, on_delete=models.CASCADE)


class Boleto(models.Model):
    plazas = models.IntegerField(default=0)
    venta = models.OneToOneField(Venta, on_delete=models.CASCADE)


class Dosificacion(models.Model):
    codigo_autorizacion = models.BigIntegerField()
    llave = models.CharField(max_length=128)
    factura_inicio = models.IntegerField()
    factura_fin = models.IntegerField()
    fecha_limite = models.DateField()
    activo = models.BooleanField(default=True)
    lugar = models.ForeignKey(Lugar, on_delete=models.CASCADE)


class Factura(models.Model):
    numero = models.IntegerField()
    ci_nit = models.CharField(max_length=12)
    nombre = models.CharField(max_length=50)
    fecha_hora = models.DateTimeField(default=datetime.datetime.now)
    importe = models.FloatField(default=0)
    venta = models.OneToOneField(Venta, on_delete=models.CASCADE)
    dosificacion = models.ForeignKey(Dosificacion, on_delete=models.CASCADE)


