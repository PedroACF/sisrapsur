from django.urls import path

from . import views

urlpatterns = [
    path('', views.HorarioList.as_view(), name='horario.lista'),
    path('asignacion-eliminar', views.asignacion_eliminar, name='asignacion.eliminar'),
    path('asignacion-crear', views.asignacion_crear, name='asignacion.crear'),
    path('crear', views.horario_crear, name='horario.crear'),
    path('turno', views.turnos, name='horario.turnos'),
    path('suplente', views.suplente, name='secretaria.suplente'),
    path('suplente/<int:sup_pk>', views.delegar_suplencia, name='secretaria.suplente.delegar'),
    path('mostrar', views.horario_mostrar, name='horario.mostrar'),
    path('<int:pk>/editar', views.horario_editar, name='horario.editar'),
    path('<int:pk>/eliminar', views.horario_eliminar, name='horario.eliminar'),
]
