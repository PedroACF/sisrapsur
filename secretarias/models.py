import datetime
from django.db import models
from main.models import Secretaria


class Horario(models.Model):
    turno = models.CharField(max_length=30, unique=True)
    hora_ingreso = models.TimeField()
    hora_salida = models.TimeField()


class AsignacionHorario(models.Model):
    fecha = models.DateField()
    hora_ingreso_reg = models.TimeField(blank=True, null=True)
    hora_salida_reg = models.TimeField(blank=True, null=True)
    hora_ingreso = models.TimeField()
    hora_salida = models.TimeField()
    horario = models.ForeignKey(Horario, on_delete=models.CASCADE)
    empleado = models.ForeignKey(Secretaria, on_delete=models.CASCADE, related_name='asignaciones')
    id_suplente = models.IntegerField(default=0)

