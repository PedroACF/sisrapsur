from django.shortcuts import render, redirect, get_object_or_404
from django.views.generic import ListView
from django.http import JsonResponse
from .models import Horario, AsignacionHorario
from main.models import Persona, Usuario
from .forms import HorarioForm, SuplenciaForm, SuplenteForm
import datetime


class HorarioList(ListView):
    model = Horario
    template_name = 'horario/lista.html'


def horario_crear(request):
    if request.method == 'POST':
        form = HorarioForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('horario.lista')
    else:
        form = HorarioForm()
    return render(request, 'horario/form.html', {'form': form})


def horario_editar(request, pk):
    horario = get_object_or_404(Horario, pk=pk)
    if request.method == 'POST':
        form = HorarioForm(request.POST, instance = horario)
        if form.is_valid():
            form.save()
            return redirect('horario.lista')
    else:
        form = HorarioForm(instance = horario)
    return render(request, 'horario/form.html', {'form': form})


def horario_eliminar(request, pk):
    horario = get_object_or_404(Horario, pk=pk)
    horario.delete()
    return JsonResponse({'success': True})


def horario_mostrar(request):
    ci = request.GET.get('ci', '')
    inicio = request.GET.get('inicio', '')
    fin = request.GET.get('fin', '')
    try:
        persona = Persona.objects.get(ci=ci)
    except Persona.DoesNotExist:
        return JsonResponse({'success': False, 'message': 'No existen secretarias con este ci'})
    if not (hasattr(persona, 'usuario') and hasattr(persona.usuario, 'secretaria')):
        return JsonResponse({'success': False, 'message': 'No existen secretarias con este ci'})
    nombre = persona.nombres + ' ' + persona.apellidos
    turnos = []
    secretaria = persona.usuario.secretaria
    for asig in secretaria.asignaciones.filter(fecha__range=(inicio, fin)):
        newT = {
                'id': asig.id,
                'title': asig.horario.turno,
                'start': "%sT%s" % (asig.fecha.strftime("%Y-%m-%d"), asig.hora_ingreso.strftime("%H:%M")),
                'end': "%sT%s" % (asig.fecha.strftime("%Y-%m-%d"), asig.hora_salida.strftime("%H:%M")),
                'color': 'green'
                }
        turnos.append(newT)
    return JsonResponse({'success': True, 'nombre': nombre, 'turnos': turnos})


def asignacion_eliminar(request):
    id = request.GET.get('id', 0)
    asig = get_object_or_404(AsignacionHorario, pk=id)
    asig.delete()
    return JsonResponse({'success': True})


def asignacion_crear(request):
    ci = request.GET.get('ci', '')
    fecha = request.GET.get('fecha', '')
    id_horario = request.GET.get('id_horario', 0)
    horario = Horario.objects.get(pk=id_horario)
    persona = Persona.objects.get(ci=ci)
    secretaria = persona.usuario.secretaria
    flag = False
    try:
        current = AsignacionHorario.objects.filter(empleado__lugar=secretaria.lugar, fecha=fecha, horario=horario).get()
        return JsonResponse({'message':'Ya existe un empleado asignado a este turno', 'success': False})
    except AsignacionHorario.DoesNotExist:
        flag = True
    asignacion = AsignacionHorario(empleado=secretaria, fecha=fecha, horario=horario, hora_ingreso=horario.hora_ingreso, hora_salida=horario.hora_salida)
    asignacion.save()
    return JsonResponse({'success': True})


def turnos(request):
    context = {'horarios': Horario.objects.all()}
    return render(request, 'turnos/turnos.html', context)


def suplente(request):
    form = SuplenteForm(initial={'current_user_id': request.user.id})
    if request.method == 'POST':
        form = SuplenteForm(request.POST)
        if form.is_valid():
            suplente = form.get_suplente()
            return redirect('secretaria.suplente.delegar', sup_pk=suplente.id)
    return render(request, 'suplencia.html', {'form': form})


def delegar_suplencia(request, sup_pk):
    current_user = request.user
    alt_user = Usuario.objects.get(pk=sup_pk)
    form = SuplenciaForm(initial={
        'current_user': current_user.usuario,
        'current_huella': current_user.codigo_huella,
        'alt_user': alt_user.usuario,
        'alt_huella': alt_user.codigo_huella
    })
    if request.method == 'POST':
        form = SuplenciaForm(request.POST)
        if form.is_valid():
            asignaciones = current_user.secretaria.asignaciones.filter(fecha=datetime.date.today())
            if asignaciones.exists():
                asig = asignaciones[0]
                asig.id_suplente = alt_user.pk
                asig.save()
            return redirect('/')
    return render(request, 'delegar_suplente.html', {'form': form, 'current': current_user.usuario, 'alt': alt_user.usuario})

