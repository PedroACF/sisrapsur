from django import forms
from django.forms import ModelForm, TextInput, ValidationError
from .models import Horario
from main.models import Usuario
from django.contrib.auth import authenticate


class HorarioForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if self.instance.pk is not None:
            self.fields['turno'].widget.attrs.update({'readonly': True})

    class Meta:
        model = Horario
        fields = ['turno', 'hora_ingreso', 'hora_salida']
        widgets = {
            'turno': TextInput(attrs={'class': 'form-control upper'}),
            'hora_ingreso': TextInput(attrs={'class': 'form-control clockpicker'}),
            'hora_salida': TextInput(attrs={'class': 'form-control clockpicker'}),
        }


class SuplenciaForm(forms.Form):
    current_user = forms.CharField(widget=forms.HiddenInput)
    current_huella = forms.CharField(required=False, widget=forms.HiddenInput(attrs={'disabled': True}))
    current_password = forms.CharField(required=False, widget=forms.PasswordInput(attrs={'class': 'form-control',
                                                                                         'style': 'margin-top: 10px'}))

    alt_user = forms.CharField(widget=forms.HiddenInput)
    alt_huella = forms.CharField(required=False, widget=forms.HiddenInput(attrs={'disabled': True}))
    alt_password = forms.CharField(required=False, widget=forms.PasswordInput(attrs={'class': 'form-control',
                                                                                     'style': 'margin-top: 10px'}))

    def clean_current_password(self):
        password = self.cleaned_data.get('current_password')
        user = self.get_current_user()
        if user is None:
            raise ValidationError('Credenciales incorrectas.')
        return password

    def clean_alt_password(self):
        password = self.cleaned_data.get('alt_password')
        user = self.get_alt_user()
        if user is None:
            raise ValidationError('Credenciales incorrectas.')
        return password

    def get_current_user(self):
        username = self.cleaned_data.get('current_user')
        password = self.cleaned_data.get('current_password')
        huella = self.cleaned_data.get('current_huella')
        return self.get_user(username, password, huella)

    def get_alt_user(self):
        username = self.cleaned_data.get('alt_user')
        password = self.cleaned_data.get('alt_password')
        huella = self.cleaned_data.get('alt_huella')
        return self.get_user(username, password, huella)

    def get_user(self, username, password, huella):
        user = authenticate(usuario=username, password=password)
        if user is None:
            try:
                user = Usuario.objects.get(usuario=username, codigo_huella=huella)
                return user
            except (Usuario.DoesNotExist, Usuario.MultipleObjectsReturned) as e:
                return None
        return user


class SuplenteForm(forms.Form):
    current_user_id = forms.CharField(widget=forms.HiddenInput)
    usuario = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'}))

    def clean_usuario(self):
        user_id = self.cleaned_data.get('current_user_id')
        nombre = self.cleaned_data.get('usuario')
        try:
            user = Usuario.objects.get(usuario=nombre)
        except (Usuario.DoesNotExist, Usuario.MultipleObjectsReturned) as e:
            user = None
        if user is None:
            raise ValidationError('Usuario inexistente o credenciales incorrectas.')
        if str(user_id) == str(user.id):
            raise ValidationError('El mismo usuario no puede hacer suplencia')
        if not (hasattr(user, 'afiliado') or hasattr(user, 'secretaria')):
            raise ValidationError('Solo un afiliado u otra secretaria puede hacer suplencia')
        return nombre

    def get_suplente(self):
        nombre = self.cleaned_data.get('usuario')
        user = Usuario.objects.get(usuario=nombre)
        return user

