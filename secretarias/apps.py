from django.apps import AppConfig


class SecretariasConfig(AppConfig):
    name = 'secretarias'
