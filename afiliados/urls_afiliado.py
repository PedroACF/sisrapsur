from django.urls import path

from . import views

urlpatterns = [
    path('', views.AfiliadoList.as_view(), name='afiliado.lista'),
    path('crear', views.afiliado_crear, name='afiliado.crear'),
    path('estado', views.AfiliadoEstado.as_view(), name='afiliado.estado'),
    path('cambiar-estado', views.afiliado_cambiar_estado, name='afiliado.cambiar_estado'),
    path('<int:pk>/editar-datos', views.afiliado_editar_datos, name='afiliado.editar_datos'),
    path('<int:pk>/editar-credenciales', views.afiliado_editar_credenciales, name='afiliado.editar_credenciales'),
    path('<int:pk>/eliminar', views.afiliado_eliminar, name='afiliado.eliminar'),
]
