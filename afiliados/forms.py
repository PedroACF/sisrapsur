from django.forms import ModelForm, TextInput, FileInput
from .models import Automovil


class AutomovilForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if self.instance.pk is not None:
            self.fields['placa'].widget.attrs.update({'readonly': True})

    class Meta:
        model = Automovil
        fields = ['placa', 'marca', 'tipo', 'clase', 'plazas', 'color', 'foto']
        widgets = {
            'placa': TextInput(attrs={'class': 'form-control upper'}),
            'marca': TextInput(attrs={'class': 'form-control upper'}),
            'tipo': TextInput(attrs={'class': 'form-control upper'}),
            'clase': TextInput(attrs={'class': 'form-control upper'}),
            'plazas': TextInput(attrs={'class': 'form-control upper only-numbers'}),
            'color': TextInput(attrs={'class': 'form-control upper'}),
            'foto': FileInput(attrs={'class': 'form-control'})
        }

