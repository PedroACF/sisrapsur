from django.urls import path
from . import views


urlpatterns = [
    path('', views.AutomovilList.as_view(), name='automovil.lista'),
    path('crear', views.automovil_crear, name='automovil.crear'),
    path('<int:pk>', views.AutomovilDetail.as_view(), name='automovil.mostrar'),
    path('<int:pk>/editar', views.automovil_editar, name='automovil.editar'),
    path('<int:pk>/eliminar', views.automovil_eliminar, name='automovil.eliminar'),
    path('<int:pk>/ligar', views.automovil_link, name='automovil.ligar')
]