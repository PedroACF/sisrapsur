import uuid
from os import path
from django.db import models
from main.models import Afiliado


def content_filename(instance, filename):
    ext = filename.split('.')[-1]
    filename = "%s.%s" % (str(uuid.uuid4().hex), ext)
    return path.join('fotos_vehiculos', filename)


class Automovil(models.Model):
    placa = models.CharField(max_length=10, unique=True)
    marca = models.CharField(max_length=30)
    tipo = models.CharField(max_length=30)
    clase = models.CharField(max_length=30)
    plazas = models.IntegerField(default=0)
    color = models.CharField(max_length=30)
    foto = models.ImageField(upload_to=content_filename)
    conductor = models.OneToOneField(Afiliado, on_delete=models.SET_NULL, blank=True, null=True)


