from django.shortcuts import render, redirect, get_object_or_404
from django.views.generic import ListView, DetailView
from django.http import JsonResponse
from django.db import transaction
from .forms import AutomovilForm
from main.forms import UsuarioForm, PersonaForm
from .models import Automovil
from main.models import Afiliado, Persona


class AutomovilList(ListView):
    model = Automovil
    template_name = 'automovil/lista.html'


class AutomovilDetail(DetailView):
    model = Automovil
    template_name = 'automovil/detalle.html'


def automovil_crear(request):
    if request.method == 'POST':
        form = AutomovilForm(request.POST, request.FILES)
        if form.is_valid():
            auto = form.save()
            return redirect('automovil.mostrar', pk=auto.id)
    else:
        form = AutomovilForm()
    return render(request, 'automovil/form.html', {'form': form})


def automovil_editar(request, pk):
    automovil = get_object_or_404(Automovil, pk=pk)
    if request.method == 'POST':
        form = AutomovilForm(request.POST, request.FILES, instance=automovil)
        if form.is_valid():
            auto = form.save()
            return redirect('automovil.mostrar', pk=auto.id)
    else:
        form = AutomovilForm(instance=automovil)
    return render(request, 'automovil/form.html', {'form': form})


def automovil_eliminar(request, pk):
    automovil = get_object_or_404(Automovil, pk=pk)
    automovil.delete()
    return JsonResponse({'success': True})


def automovil_link(request, pk):
    automovil = get_object_or_404(Automovil, pk=pk)
    ligar = request.POST['ligar']
    ci = request.POST['ci']
    if ligar == 'true':
        try:
            persona = Persona.objects.get(ci=ci)
        except (Persona.DoesNotExist, Persona.MultipleObjectsReturned) as e:
            persona = None
        if persona is None:
            return JsonResponse({'error': 'No se ha encontrado afiliado con este ci', 'success': False})
        usuario = persona.usuario
        if not hasattr(usuario, 'afiliado'):
            return JsonResponse({'error': 'La persona con este ci no es afiliado', 'success': False})
        afiliado = usuario.afiliado
        if hasattr(afiliado, 'automovil'):
            return JsonResponse({'error': 'Este afiliado ya esta vinculado a un vehiculo', 'success': False})
        automovil.conductor = afiliado
        automovil.save()
        return JsonResponse({'success': True})
    else:
        print("desligando")
        automovil.conductor = None
        automovil.save()
        return JsonResponse({'success': True})


# Vistas para secretarias
class AfiliadoList(ListView):
    model = Afiliado
    template_name = 'afiliado/lista.html'


@transaction.atomic
def afiliado_crear(request):
    if request.method == 'POST':
        user_form = UsuarioForm(request.POST)
        pers_form = PersonaForm(request.POST)
        if user_form.is_valid() and pers_form.is_valid():
            user = user_form.save(commit=False)
            user.tipo = 'AFILIADO'
            user.save()
            persona = pers_form.save(commit=False)
            persona.usuario = user
            persona.save()
            afili = Afiliado(usuario = user)
            afili.save()
            return redirect('afiliado.lista')
    else:
        user_form = UsuarioForm()
        pers_form = PersonaForm()
    return render(request, 'afiliado/crear.html', {'user_form': user_form, 'pers_form': pers_form})


@transaction.atomic
def afiliado_editar_datos(request, pk):
    afiliado = get_object_or_404(Afiliado, pk=pk)
    usuario = afiliado.usuario
    persona = usuario.persona
    if request.method == 'POST':
        form = PersonaForm(request.POST, instance = persona)
        if form.is_valid():
            form.save()
            return redirect('afiliado.lista')
    else:
        form = PersonaForm(instance = persona)
    return render(request, 'afiliado/editar_datos.html', {'pers_form': form})


@transaction.atomic
def afiliado_editar_credenciales(request, pk):
    afiliado = get_object_or_404(Afiliado, pk=pk)
    usuario = afiliado.usuario
    if request.method == 'POST':
        form = UsuarioForm(request.POST, instance = usuario)
        if form.is_valid():
            form.save()
            return redirect('afiliado.lista')
    else:
        form = UsuarioForm(instance = usuario)
    return render(request, 'afiliado/editar_credenciales.html', {'user_form': form})


@transaction.atomic
def afiliado_eliminar(request, pk):
    afiliado = get_object_or_404(Afiliado, pk=pk)
    usuario = afiliado.usuario
    usuario.delete()
    return JsonResponse({'success': True})


def afiliado_cambiar_estado(request):
    if request.method == 'POST':
        pk = request.POST["pk"]
        nuevo_estado = request.POST['estado']
        afiliado = get_object_or_404(Afiliado, pk=pk)
        afiliado.estado = nuevo_estado
        afiliado.save()
        return JsonResponse({'success': True})
    return JsonResponse({'success': False})


class AfiliadoEstado(ListView):
    model = Afiliado
    template_name = 'afiliado/estado.html'